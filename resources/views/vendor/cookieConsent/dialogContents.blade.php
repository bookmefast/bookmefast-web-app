<div class="js-cookie-consent cookie-consent">
 <div class="alert alert-warning text-center" role="alert">
    <span class="cookie-consent__message">
        {!! trans('cookieConsent::texts.message') !!}
    </span>

    <button class="js-cookie-consent-agree cookie-consent__agree">
        {{ trans('cookieConsent::texts.agree') }}
    </button>
</div>
</div>
