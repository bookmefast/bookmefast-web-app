@extends('layouts.panel')
@section('title')
Customer List
@parent
@stop

@section('content')

<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <strong> Customers</strong>
                </div>
                <div class="card-body">
                    @if($customers->count() < 1) <div class="alert alert-warning">Sorry there is no customers!</div>
                @else
                <div class="table-responsive-sm">
                    <table class="table table-striped table-hover">
                        <thead class="thead-header">
                            <tr>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th class="text-right"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($customers as $customer)
                            <tr>
                                <td>{{ $customer->name }}</td>
                                <td>{{ $customer->phone }}</td>
                                <td>{{ $customer->email }}</td>
                                <td class="text-right">
                                    <a class="btn btn-sm btn-primary"
                                        href="{{ route('backoffice.customers.appointments', $customer->id) }}">
                                        <i class="align-middle feather_small" data-feather="book"></i>
                                        Appointments</a>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')


@stop