@extends('layouts.panel')
@section('title')
Appointments
@parent
@stop

@section('content')

<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <strong> Appointments</strong>

                    <a class="btn btn-primary btn-sm float-right" href="{{ route('backoffice.customers.index') }}"><i
                            class="align-middle feather_small" data-feather="list"></i> Customers List </a>

                </div>
                <div class="card-body">
                    @if($appointments->count() < 1) <div class="alert alert-warning">Sorry there is no appointment!
                </div>
                @else
                <div class="table-responsive-sm">
                    <table class="table table-striped table-hover">
                        <thead class="thead-header">
                            <tr>
                                <th>Appointment At</th>
                                <th>Service</th>
                                <th>Staff</th>
                                <th>Customer Name</th>
                                <th>Customer Email</th>
                                <th>Customer Phone</th>
                                <th>Verified</th>
                                <th>Created At</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($appointments as $app)
                            <tr>
                                <td>{!! $app->appointment_time !!}</td>
                                <td>{{ $app->service->name }}</td>
                                <td>{{ $app->staff->name }}</td>
                                <td>{{ $app->customer_name }}</td>
                                <td>{{ $app->customer_email }}</td>
                                <td>{{ $app->customer_phone }}</td>
                                <td>{{ $app->is_verified }}</td>
                                <td>{{ Carbon\Carbon::parse($app->created_at)->format('d M Y H:i') }}</td>
                                <td>
                                    @if(Auth::user()->id == $app->user_id || Auth::user()->hasRole('owner'))
                                    <a class="btn btn-sm btn-primary"
                                        href="{{ route('backoffice.appointments.view', $app->id) }}">
                                        <i class="align-middle feather_small" data-feather="eye"></i>
                                        {{ trans('labels.view') }}</a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
</div>


@endsection
@section('scripts')


@stop