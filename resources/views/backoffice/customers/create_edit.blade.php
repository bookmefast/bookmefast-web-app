@extends('layouts.panel')
@section('title')
Customer Details
@parent
@stop
@section('content')

<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title float-left"> Customer Information</h4>

                    <a class="btn btn-primary btn-sm float-right" href="{{ route('backoffice.customers.index') }}"><i
                            class="align-middle feather_small" data-feather="list"></i> Customers List </a>
                </div>
                <div class="card-body">
                    <form autocomplete="off" role="form"
                        action="{{ (isset($customer)) ? route('backoffice.customers.update', $customer->id ) : route('backoffice.customers.store') }}"
                        method="POST">
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

                        @if(isset($customer))
                        <input name="_method" type="hidden" value="PUT">
                        @endif

                        <div class="form-group row {!! $errors->first('name', 'has-warning') !!}">
                            <label for="name" class="col-sm-2 col-form-label">{{ trans('labels.name') }}
                                {{ old('name')
                                }}</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="name" name="name" readonly
                                    autocomplete="off"
                                    value="{{{ old('name', isset($customer) ? $customer->name : null) }}}">
                                {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                            </div>

                            <label for="email" class="col-sm-2 col-form-label">{{ trans('labels.email') }}
                                {{
                                old('email') }}</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="email" name="email" readonly
                                    autocomplete="off"
                                    value="{{{ old('email', isset($customer) ? $customer->email : null) }}}">
                                {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="form-group row {!! $errors->first('phone', 'has-warning') !!}">
                            <label for="phone" class="col-sm-2 col-form-label">{{ trans('labels.phone') }}
                                {{
                                old('phone') }}</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="phone" name="phone" readonly
                                    autocomplete="off"
                                    value="{{{ old('phone', isset($customer) ? $customer->phone : null) }}}">
                                {!! $errors->first('phone', '<span class="help-block">:message</span>') !!}
                            </div>

                            <label for="postcode" class="col-sm-2 col-form-label">{{ trans('labels.postcode') }}
                                {{
                                old('postcode') }}</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="postcode" name="postcode" readonly
                                    autocomplete="off"
                                    value="{{{ old('postcode', isset($customer) ? $customer->postcode : null) }}}">
                                {!! $errors->first('postcode', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>


                        <div class="form-group row {!! $errors->first('address', 'has-warning') !!}">
                            <label for="address" class="col-sm-2 col-form-label">{{ trans('labels.address') }}
                                {{
                                old('address') }}</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="address" name="address" readonly
                                    autocomplete="off"
                                    value="{{{ old('address', isset($customer) ? $customer->address : null) }}}">
                                {!! $errors->first('address', '<span class="help-block">:message</span>') !!}
                            </div>

                            <label for="city" class="col-sm-2 col-form-label">{{ trans('labels.city') }}
                                {{ old('city')
                                }}</label>
                            <div class="col-sm-4">
                                {{ Form::select('city', $cities, old('city', isset($customer) ? $customer->city :
                                null), ['class' => 'cities form-control', 'id' => 'cities', 'readonly' => 'readonly']) }}
                                {!! $errors->first('city', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

    @endsection
    @section('scripts')

    <script>
    if ($('#divisions option').length == 0) {
        $('.divisions').hide();
    }

    if ($('#cities option').length == 0) {
        $('.city').hide();
    }

    $('#countries').on('change', function() {
        $('#cities').empty();
        $('#divisions').empty();
        $('.divisions').hide();
        $('.city').hide();

        var country = $('#countries').val();
        var url_country_cities = "";
        url_country_cities = "/api/helper/countries/%s/cities";
        url_country_states = "/api/helper/countries/%s/divisions";

        url_country_states = url_country_states.replace("%s", country);
        url_country_cities = url_country_cities.replace("%s", country);

        $.get(url_country_states)
            .done(function(data) {
                if (data.data.length > 0) {
                    $('.divisions').show(1000);
                    $('#divisions').append($('<option>', {
                        value: 0,
                        text: "- - - "
                    }));
                    $.each(data.data, function(i, item) {
                        $('#divisions').append($('<option>', {
                            value: item.id,
                            text: item.name
                        }));
                    })
                } else {
                    $.get(url_country_cities)
                        .done(function(data) {
                            if (data.data.length > 0) {
                                $('.city').show(1000);
                                $('#cities').append($('<option>', {
                                    value: 0,
                                    text: "- - - "
                                }));

                                $.each(data.data, function(i, item) {
                                    $('#cities').append($('<option>', {
                                        value: item.id,
                                        text: item.name
                                    }));
                                })
                            }
                        });
                }
            });
    });

    $('#divisions').on('change', function() {
        $('#cities').empty();

        var country = $('#countries').val();
        var division = $('#divisions').val();

        var url = "api/helper/countries/%country/divisions/%division/cities";
        url = url.replace("%country", country);
        url = url.replace("%division", division);
        $.get(url)
            .done(function(data) {
                if (data.data.length > 0) {
                    $('.city').show(1000);
                    $('#cities').append($('<option>', {
                        value: 0,
                        text: "- - - "
                    }));
                    $.each(data.data, function(i, item) {
                        $('#cities').append($('<option>', {
                            value: item.id,
                            text: item.name
                        }));
                    })
                }
            });

    });
    </script>

    @stop