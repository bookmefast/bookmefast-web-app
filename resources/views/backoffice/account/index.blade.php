@extends('layouts.panel')
@section('title')
Account
@parent
@stop
@section('css')
<link href="{{ mix('css/summernote-lite.css') }}" rel="stylesheet">
@stop
@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <strong> Account</strong>
                </div>
                <div class="card-body">
                    @include('backoffice.partials.notifications')
                    <div class="row">
                        <div class="col-sm-10 offset-sm-1">
                            <form autocomplete="off" role="form" enctype="multipart/form-data"
                                action="{{ route('backoffice.account.update') }}" method="POST">
                                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

                                <div class="form-group {!! $errors->first('name', 'has-warning') !!}">
                                    <label for="name">{{ trans('labels.name') }} {{ old('name')  }}</label>
                                    <input type="text" class="form-control" id="name" name="name" autocomplete="off"
                                        value="{{{ old('name', isset($company) ? $company->name : null) }}}">
                                    {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                                </div>
 
                                <div class="form-group {!! $errors->first('duration', 'has-warning') !!}">
                                    <label for="duration" >Template
                                      </label>
                                  
                                        {{ Form::select('template', $templates, $company->template_name, ['class'=>'form-control col-sm-3']) }}
                                        {!! $errors->first('template', '<span class="help-inline">:message</span>') !!}
                                     
                                </div>                                
 
                                <div class="form-group {!! $errors->first('description', 'has-warning') !!}">
                                    <label for="description">{{ trans('labels.description') }}
                                        {{ old('description')  }}</label>
                                    <textarea class="form-control" id="description"
                                        name="description">{{{ old('description', isset($company) ? $company->description : null) }}}</textarea>
                                    {!! $errors->first('description', '<span class="help-block">:message</span>') !!}
                                </div>

                                <div class="form-group {!! $errors->first('address', 'has-warning') !!}">
                                    <label for="address">{{ trans('labels.address') }} {{ old('address')  }}</label>
                                    <input type="text" class="form-control" id="address" name="address"
                                        autocomplete="off"
                                        value="{{{ old('address', isset($company) ? $company->address : null) }}}">
                                    {!! $errors->first('address', '<span class="help-block">:message</span>') !!}
                                </div>

                                <div class="form-group {!! $errors->first('postcode', 'has-warning') !!}">
                                    <label for="postcode">{{ trans('labels.postcode') }} {{ old('postcode')  }}</label>
                                    <input type="text" class="form-control" id="postcode" name="postcode"
                                        autocomplete="off"
                                        value="{{{ old('postcode', isset($company) ? $company->postcode : null) }}}">
                                    {!! $errors->first('postcode', '<span class="help-block">:message</span>') !!}
                                </div>

                                <div class="form-group {!! $errors->first('email', 'has-warning') !!}">
                                    <label for="email">{{ trans('labels.email') }} {{ old('email')  }}</label>
                                    <input type="text" class="form-control" id="email" name="email" autocomplete="off"
                                        value="{{{ old('email', isset($company) ? $company->email : null) }}}">
                                    {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                                </div>

                                <div class="form-group {!! $errors->first('phone', 'has-warning') !!}">
                                    <label for="phone">{{ trans('labels.phone') }} {{ old('phone')  }}</label>
                                    <input type="text" class="form-control" id="phone" name="phone" autocomplete="off"
                                        value="{{{ old('phone', isset($company) ? $company->phone : null) }}}">
                                    {!! $errors->first('phone', '<span class="help-block">:message</span>') !!}
                                </div>

                                <div class="form-group {!! $errors->first('website', 'has-warning') !!}">
                                    <label for="website">{{ trans('labels.website') }} {{ old('website')  }}</label>
                                    <input type="text" class="form-control" id="website" name="website"
                                        autocomplete="off"
                                        value="{{{ old('website', isset($company) ? $company->website : null) }}}">
                                    {!! $errors->first('website', '<span class="help-block">:message</span>') !!}
                                </div>

                                <button type="submit" class="btn btn-primary pull-right">
                                    <i class="fa fa-save" aria-hidden="true"></i>
                                    {{ trans('labels.save') }}</button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ mix('js/summernote-lite.min.js') }}"></script>
<script>
$(document).ready(function() {
    $('#description').summernote({
        height: 100,
        toolbar: false,
        tabsize: 2
    });
});
</script>
@stop