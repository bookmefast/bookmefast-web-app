@extends('layouts.panel')
@section('title')
Details of appointment: #{{ $appointment->id }}
@parent
@stop
@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header container-fluid">
                    <div class="row">
                        <div class="col-md-8">
                            <strong> #{{ $appointment->id }} Appointment Details</strong>
                        </div>
                        <div class="col-md-4 ">
                            <a class="btn btn-primary btn-sm float-right"
                                href="{{ route('backoffice.appointments') }}"><i class="align-middle feather_small"
                                    data-feather="skip-back"></i> Back to appointments </a>
                        </div>
                    </div>
                </div>
                <div class="card-body appointment">

                    @include('backoffice.partials.notifications')

                    <div class="row p-3">
                        <div class="col-md-10 offset-md-1">
                            <h4>Customer:</h4>
                            <div class="row">
                                <div class="col-md-5 border-bottom p-2">
                                    <b>Name:</b>
                                </div>
                                <div class="col-md-7 border-bottom p-2">
                                    {{ $appointment->customer_name }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 border-bottom p-2">
                                    <b>Email:</b>
                                </div>
                                <div class="col-md-7 border-bottom p-2">
                                    {{ $appointment->customer_email }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 border-bottom p-2">
                                    <b>Phone:</b>
                                </div>
                                <div class="col-md-7 border-bottom p-2">
                                    {{ $appointment->customer_phone     }}
                                </div>
                            </div>
                            <br />
                            <h4>Appointment Details:</h4>
                            <div class="row">
                                <div class="col-md-5 border-bottom p-2">
                                    <b>Service:</b>
                                </div>
                                <div class="col-md-7 border-bottom p-2">
                                    {{ $appointment->service->name }}
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-5 border-bottom p-2">
                                    <b>Staff:</b>
                                </div>
                                <div class="col-md-7 border-bottom p-2">
                                    {{ $appointment->staff->name }}
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-5 border-bottom p-2">
                                    <b> App. Date:</b>
                                </div>
                                <div class="col-md-7 border-bottom p-2">

                                    {!! $appointment->appointment_time_user !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 border-bottom p-2">
                                    <b>Duration:</b>
                                </div>
                                <div class="col-md-7 border-bottom p-2">
                                    {!! $appointment->start_time->diffInMinutes($appointment->end_time) !!} min.
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-5 border-bottom p-2">
                                    <b>Created Date</b>
                                </div>
                                <div class="col-md-7 border-bottom p-2">
                                    {{ $appointment->created_at->format('d M') }},
                                    {{ $appointment->created_at->format('H:i') }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 border-bottom p-2">
                                    <b>Service Fee</b>
                                </div>
                                <div class="col-md-7 border-bottom p-2">
                                    £{!! $appointment->service->service_price !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    @if(Auth::user()->hasRole('owner') || Auth::user()->id == $appointment->user_id)
                    @if($appointment->start_time->gt($today) )

                    <div class="row m-2">
                        <div class="col-md-6">
                            @if($appointment->approved == 0)
                            <form autocomplete="off" role="form"
                                action="{{ route('backoffice.appointments.approve', $appointment->id) }}" method="post">
                                {{ csrf_field() }}
                                <button class="btn btn-primary btn-lg btn-block" type="submit">
                                    Approve Appointment
                                </button>
                            </form>
                            @else
<!--                             <form autocomplete="off" role="form"
                                action="{{ route('backoffice.appointments.update', $appointment->id) }}" method="get">
                                <button class="btn btn-primary btn-lg btn-block" type="submit">
                                    Update Appointment
                                </button>
                            </form> -->

                            <form autocomplete="off" role="form"
                                action="{{ route('backoffice.appointments.reminder', $appointment->id) }}" method="get">
                                <button class="btn btn-primary btn-lg btn-block" type="submit">
                                    Send Reminder
                                </button>
                            </form>

                            @endif
                        </div>
                        <div class="col-md-6">
                            <form autocomplete="off" role="form" class="delete_form"
                                action="{{ route('backoffice.appointments.cancel', $appointment->id) }}" method="post">
                                {{ csrf_field() }}
                                <button class="btn btn-danger btn-lg btn-block delete" type="submit">
                                    Cancel Appointment
                                </button>
                            </form>
                        </div>
                    </div>
                    @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script>
$(document).ready(function() {

    $('.delete_form').on('submit', function(e) {
        var currentForm = this;
        e.preventDefault();

        bootbox.confirm({
            message: "Do you want to cancel this lesson?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success btn-sm'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function(result) {
                if (result) {
                    currentForm.submit();
                }
            }
        });
    });
});
</script>
@stop