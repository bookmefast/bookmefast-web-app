@extends('layouts.panel')
@section('title')
Create appointment:
@parent
@stop
@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header container-fluid">
                    <div class="row">
                        <div class="col-md-8">
                            <strong> Create Appointment </strong>
                        </div>
                        <div class="col-md-4 ">

                        </div>
                    </div>
                </div>
                <div class="card-body appointment">
                    @include('backoffice.partials.notifications')
                    <div class="row">
                        <div class="col-sm-12 col-md-4">
                            <form>
                                <div class="form-group ">
                                    <label for="start_date">Date</label>
                                    <input class="form-control col-6" id="selected_date" name="book_date" />
                                </div>

                                <div class="form-group ">
                                    <label for="timezone">Select Service </label>
                                    {{ Form::select('service', $services, null, ['class'=>'form-control service col-6']) }}
                                    {!! $errors->first('service', '<span class="help-inline">:message</span>') !!}
                                </div>
                                <div class="form-group ">
                                    <label for="timezone">Show the days in </label>
                                    {{ Form::select('timezone', $timezones, $selected, ['class'=>'form-control timezone col-6']) }}
                                    {!! $errors->first('timezone', '<span class="help-inline">:message</span>') !!}
                                </div>
                                <button type="button" class="btn btn-primary availability">Show Availability</button>
                            </form>
                        </div>
                        <div class="col-sm-12  col-md-8">
                            <form id="book" action="" method="">
                                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

                                <input type="hidden" name="company" value="{{ $company->id }}" />
                                <input type="hidden" name="user" value="{{ $appointment->user_id }}" />

                                <input type="hidden" class="selected_date" name="selected_date" />
                                <input type="hidden" class="selected_time" name="selected_time" />
                                <ul id="hours" class="list-inline"></ul>
                            </form>
                        </div>
                    </div>

                    <div class="modal fade" id="clientDetails" tabindex="-1" role="dialog"
                        aria-labelledby="clientModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="clientModalLabel">Book the date</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">

                                    <form>
                                        <div class="form-group row">
                                            <label for="client_email" class="col-sm-2 col-form-label">Email</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="client_email" value="">
                                                <span class="error_email text-danger book_error"></span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="client_name" class="col-sm-2 col-form-label">Name</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="client_name" placeholder="">
                                                <span class="error_name text-danger book_error"></span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="client_phone" class="col-sm-2 col-form-label">Phone</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="client_phone"
                                                    placeholder="">
                                                <span class="error_phone text-danger book_error"></span>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary save_book">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@section('scripts')
<script type='text/javascript'>
var today = new Date();
var tomorrow = new Date();
tomorrow.setDate(today.getDate() + 1);

$('#selected_date').datepicker({
    dateFormat: "yy-mm-dd",
    minDate: tomorrow,
    maxDate: "+1m +1w"
});

$('#selected_date').on('change', function() {
    $('.selected_date').val($(this).val());
});

$('.availability').on('click', function() {
    var user = $("#book :input[name='user']").val();
    var company = $("#book :input[name='company']").val();
    var service = $(".service").val();
    var selected_date = $(".selected_date").val();
    var timezone = $('.timezone').val();
    var req = {
        user: user,
        service: service,
        company: company,
        date: selected_date,
        timezone: timezone
    }

    var url = '//api.bookmefast.co.uk/api/v1/book/check-availability';
    $.ajax({
        type: "POST",
        dataType: 'json',
        headers: {
            'Authorization': 'Basic xxxxxxxxxxxxx',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            'Content-Type': 'application/json'
        },
        url: url,
        data: JSON.stringify(req),
        success: function(data) {
            $('#hours').empty();
            timeplan = data.timePlan;
            if (timeplan.length < 1) {
                alert('There is no free slot on selected date');
                $('.closed').show();
            } else {
                $('.closed').hide();

                data.timePlan.forEach(function(res, i) {
                    if (res.available == 0) {
                        $('#hours').append(
                            '<li class="list-inline-item m-2"><button type="button" data-slot="' +
                            i + '" data-time="' + res.start + '" class="slot' + i +
                            '  book_time btn btn-primary btn-sm">' + res
                            .timezone_start + " - " +
                            res.timezone_end + '</button></li>');
                    } else {
                        $('#hours').append(
                            '<li class="list-inline-item m-2"><button type="button" data-slot="' +
                            i + '" readonly="readonly" disabled class="slot' + i +
                            ' book_time btn-warning btn btn-sm">' + res.timezone_start +
                            " - " +
                            res.timezone_end + '</button></li>');
                    }
                })
            }
        },
        error: function(data) {
            alert("Free slots can not be founded, please try again.");
        }
    });
})

$(document).on('click', '.book_time', function() {
    selected_hour = $(this).data('time');
    selected_slot = $(this).data('slot');
    $('.book_time').removeClass('btn-outline-primary').addClass('btn-primary');
    $(this).addClass('btn-outline-primary');
    $('.selected_time').val(selected_hour);
    $('#clientDetails').modal('show')
});

$(document).on('click', '.save_book', function() {
    var selected_hour = $('.selected_time').val();
    var selected_date = $('.selected_date').val();
    var client_email = $('#client_email').val();
    var client_name = $('#client_name').val();
    var client_phone = $('#client_phone').val();
    var timezone = $('.timezone').val();

    var user = $("#book :input[name='user']").val();
    var company = $("#book :input[name='company']").val();
    var service = $(".service").val();
    var req = {
        selected_hour: selected_hour,
        selected_date: selected_date,
        email: client_email,
        name: client_name,
        phone: client_phone,
        user: user,
        company: company,
        service: service,
        timezone: timezone
    }

    var url = '//api.bookmefast.co.uk/api/v1/book/save';
    $.ajax({
        type: "POST",
        dataType: 'json',
        headers: {
            'Authorization': 'Basic xxxxxxxxxxxxx',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            'Content-Type': 'application/json'
        },
        url: url,
        data: JSON.stringify(req),
        success: function(data) {
            $('.book_error').html('')
            $('#clientDetails').modal('hide')
            $('#book').hide();
            $('#thankyou').show();
        },
        error: function(data) {
            $('.book_error').html('')
            errors = data.responseJSON;
            var err = errors.errors
            for (key in err) {
                row = err[key]
                $('.error_' + key).html(row[0])
            }
        }
    });
});
</script>
@stop
