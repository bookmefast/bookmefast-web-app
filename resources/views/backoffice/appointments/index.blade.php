@extends('layouts.panel')
@section('title') Dashboard @parent @stop @section('content')
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-3">
                            <h4> Appointments</h4>
                        </div>
                        <div class="col-9 text-right">
                            <form class="float-right form-inline" action="" method="get">

                                <select id="time" name="time" class="m-1 p-1 select2_list">
                                    <option></option>
                                    <option {{ ($time === 'next') ? 'selected' : null }} value="next">Next Appointments
                                    </option>
                                    <option {{ ($time === 'previous') ? 'selected' : null }} value="previous">Previous
                                        Appointments</option>
                                </select> &nbsp;
                                <select id="approved" name="approved" class="m-1 p-1 select2_list">
                                    <option {{ ($approved == 'all') ? 'selected' : null }}></option>
                                    <option {{ ($approved === "approved") ? 'selected' : null }} value="approved">
                                        Approved
                                        Appointments</option>
                                    <option {{ ($approved === "pending") ? 'selected' : null }} value="pending">Pending
                                        Appointments</option>
                                </select>
                                <button type="submit" class="btn btn-primary">
                                    Filter
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="card-body">

                    @if($appointments->count() < 1) <div class="alert alert-warning">Sorry there is no appointment!
                </div>
                @else
                <div class="table-responsive-sm">
                    <table class="table table-striped table-hover">
                        <thead class="thead-header">
                        <tr>
                                <th style="width:15%">Booked At</th>
                                <th style="width:15%">Service</th>
                                <th style="width:20%">Emp.</th>
                                <th style="width:20%">Name</th>
                                <th style="width:5%">Approved</th>
                                <th style="width:15%">Created At</th>
                                <th style="width:10%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($appointments as $app)
                            <tr>
                                <td>{!! $app->appointment_time_user !!}</td>
                                <td>{{ $app->service->name }}</td>
                                <td>{{ $app->staff->name }}</td>
                                <td>{{ $app->customer_name }}</td>
                                <td><span class="{{ $app->is_approved_style }}">{{ $app->is_approved }}</span></td>
                                <td>{{ Carbon\Carbon::parse($app->created_at)->format('d M Y H:i') }}</td>
                                <td><a class="btn btn-sm btn-primary btn-flat m-b-10 m-l-5"
                                        href="{{ route('backoffice.appointments.view', $app->id) }}">
                                        <i class="fas fa-eye"></i>
                                        {{ trans('labels.view') }}</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
 
                {{ $appointments->render() }}
                @endif
            </div>
        </div>
    </div>
</div>
</div>
@endsection @section('scripts') @stop