@extends('layouts.panel')
@section('title')
Update User Photo
@parent
@stop
@section('content')

<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header container-fluid">
                    <div class="row">
                        <div class="col-md-10">
                            <strong>
                                Update User Photo
                            </strong>
                        </div>
                        <div class="col-md-2 float-right">
                            <a class="btn btn-sm btn-primary float-right" href="{{ route('backoffice.team.index') }}">
                                <i class="align-middle feather_small" data-feather="list"></i>
                                List Users</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    @include('backoffice.partials.notifications')
                    <div class="row">
                        <div class="col-sm-10 offset-sm-1">
                            <form autocomplete="off" role="form"
                                action="{{ route('backoffice.team.upload_photo', $user->id ) }}" method="POST"
                                enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

                                <img src="{{ Storage::url('users/thump_'.$user->image . '.jpg')  }}"
                                    class="img-fluid" />

                                <div class="form-group {!! $errors->first('image', 'has-warning') !!}">
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" name="image" class="custom-file-input"
                                                id="inputGroupFile04" aria-describedby="inputGroupFileAddon04">
                                            <label class="custom-file-label" for="inputGroupFile04">Choose file</label>
                                        </div>
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-secondary" type="button"
                                                id="inputGroupFileAddon04">Upload</button>
                                        </div>
                                    </div>
                                    {!! $errors->first('image', '<span class="help-block">:message</span>') !!}
                                </div>


                                <button type="submit" class="btn btn-primary pull-right">
                                    <i class="fa fa-save" aria-hidden="true"></i>
                                    {{ trans('labels.save') }}</button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('scripts')
<script>
$(document).ready(function() {

});
</script>
@stop