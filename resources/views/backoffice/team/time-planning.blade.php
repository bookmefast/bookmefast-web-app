@extends('layouts.panel')
@section('title')
Time Planning
@parent
@stop
@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <strong> Time Planning for {{ $user->name }}</strong>
                </div>
                <div class="card-body">
                    @include('backoffice.partials.notifications')

                    <div class="row">
                        <div class="col-sm-4 col-md-8 offset-md-2">
                            <form autocomplete="off" role="form" action="{{ route('backoffice.team.time_plan_update', $user->id) }}"
                                method="POST">
                                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

                                @foreach($days as $day)
                                <div class="form-group {!! $errors->first($day, 'has-warning') !!} ">
                                    <label for="name"
                                        class="week-day"><strong>{{ trans("labels.$day") }}</strong></label>

                                    <div class="form-group {!! $errors->first(" {$day}", 'has-warning' ) !!} row">
                                        <label for="name"
                                            class="col-sm-3 col-form-label">{{ trans("labels.start_time") }} </label>
                                        <div class="col-sm-2">
                                            <?php
                                                    $temp = $working_hours->firstWhere('day', $day);
                                                    ?>
                                            {{ Form::select("{$day}_start_time", $hours, $temp['start_time'], ['class'=>'form-control']) }}
                                            {!! $errors->first("{$day}_start_time", '<span
                                                class="help-inline">:message</span>') !!}
                                        </div>

                                        <label for="name" class="col-sm-3 col-form-label">{{ trans("labels.end_time") }}
                                        </label>
                                        <div class="col-sm-2">
                                            {{ Form::select("{$day}_end_time", $hours, $temp['end_time'], ['class'=>'form-control']) }}
                                            {!! $errors->first("{$day}_end_time", '<span
                                                class="help-inline">:message</span>') !!}
                                        </div>

                                    </div>

                                </div>
                                @endforeach

                                <button type="submit" class="btn btn-primary pull-right">
                                    <i class="fa fa-save" aria-hidden="true"></i>
                                    {{ trans('labels.save') }}</button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')

@stop