@extends('layouts.panel')
@section('title')
User Management
@parent
@stop
@section('css')
<link href="{{ mix('css/summernote-lite.css') }}" rel="stylesheet">
@stop
@section('content')

<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header container-fluid">
                    <div class="row">
                        <div class="col-md-10">
                            <strong>
                                @if(isset($user))
                                Update user
                                @else
                                Create user
                                @endif
                            </strong>
                        </div>
                        <div class="col-md-2 float-right">
                            <a class="btn btn-sm btn-primary float-right" href="{{ route('backoffice.team.index') }}">
                                <i class="align-middle feather_small" data-feather="list"></i>
                                List Users</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    @include('backoffice.partials.notifications')
                    <div class="row">
                        <div class="col-sm-10 offset-sm-1">
                            <form autocomplete="off" role="form"
                                action="{{ (isset($user)) ? route('backoffice.team.update', $user->id ) : route('backoffice.team.store') }}"
                                method="POST">
                                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

                                <div class="form-group row {!! $errors->first('name', 'has-warning') !!}">
                                    <label for="name" class="col-sm-4 col-form-label">{{ trans('labels.name') }}
                                        </label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="name" name="name" autocomplete="off"
                                            value="{{{ old('name', isset($user) ? $user->name : null) }}}">
                                        {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="form-group row {!! $errors->first('timezones', 'has-warning') !!}">
                                    <label for="timezone" class="col-sm-4 col-form-label">Timezone</label>
                                    <div class="col-sm-8">
                                        {{ Form::select('timezone', $timezones, (isset($user) ? $user->timezone : $selected), ['class'=>'form-control col-sm-3']) }}
                                        {!! $errors->first('timezone', '<span class="help-inline">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="form-group row {!! $errors->first('email', 'has-warning') !!}">
                                    <label for="email" class="col-sm-4 col-form-label">{{ trans('labels.email') }}
                                        </label>
                                    <div class="col-sm-8"><input type="text" class="form-control" id="email"
                                            name="email" autocomplete="off"
                                            value="{{{ old('email', isset($user) ? $user->email : null) }}}">
                                        {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="form-group row {!! $errors->first('description', 'has-warning') !!}">
                                    <label for="description"
                                        class="col-sm-4 col-form-label">{{ trans('labels.description') }}
                                       </label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control" id="description"
                                            name="description">{{{ old('description', isset($user) ? $user->description : null) }}}</textarea>
                                        {!! $errors->first('descripton', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="form-group row {!! $errors->first('password', 'has-warning') !!}">
                                    <label for="password"
                                        class="col-sm-4 col-form-label">{{ trans('labels.password') }}</label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control" id="password" name="password" />
                                        {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                                <div
                                    class="form-group row {!! $errors->first('password_confirmation', 'has-warning') !!}">
                                    <label for="password_confirmation"
                                        class="col-sm-4 col-form-label">{{ trans('labels.password_confirmation') }}</label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control" id="password_confirmation"
                                            name="password_confirmation" />
                                        {!! $errors->first('password_confirmation', '<span
                                            class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary pull-right">
                                    <i class="fa fa-save" aria-hidden="true"></i>
                                    {{ trans('labels.save') }}</button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('scripts')
<script src="{{ mix('js/summernote-lite.min.js') }}"></script>

<script>
$(document).ready(function() {
    $('#description').summernote({
        height: 100,
        toolbar: false,
        tabsize: 2
    });
});
</script>
@stop