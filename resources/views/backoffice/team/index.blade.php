@extends('layouts.panel')
@section('title')
Users
@parent
@stop

@section('content')

<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header container-fluid">
                    <div class="row">
                        <div class="col-md-10">
                            <strong>
                                @if(isset($user))
                                Modify User
                                @else
                                Create User
                                @endif
                            </strong>
                        </div>
                        <div class="col-md-2 float-right">
                            <a class="btn btn-sm btn-primary float-right" href="{{ route('backoffice.team.create') }}">
                                <i class="align-middle feather_small" data-feather="list"></i>
                                Create User</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">

                    @if($employees->count() < 1) <div class="alert alert-danger mt-4">
                        Sorry, you do not have any employee
                </div>
                @else
                <div class="table-responsive-sm">
                    <table class="table">
                        <thead class="thead-header">
                            <tr>
                                <th>Name / Email</th>
                                <th class="text-right"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($employees as $employee)
                            <tr>
                                <td>{{ $employee->name }}<br />
                                   <i> {{ $employee->email }}</i>
                                </td>
                                <td class="text-right">
                                    <a class="btn btn-sm btn-primary"
                                        href="{{ route('backoffice.team.upload', $employee->id) }}">
                                        <i class="fas fa-upload"></i> Upload</a>

                                    <a class="btn btn-sm btn-primary"
                                        href="{{ route('backoffice.team.time_plan', $employee->id) }}">
                                        <i class="far fa-clock"></i> Time Plan</a>
                                    <a class="btn btn-sm btn-primary"
                                        href="{{ route('backoffice.team.edit', $employee->id) }}">
                                        <i class="align-middle feather_small" data-feather="edit"></i> Edit</a>
                                    <a class="btn btn-sm btn-primary"
                                        href="{{ route('backoffice.team.appointments', $employee->id) }}">
                                        <i class="align-middle feather_small" data-feather="book"></i>
                                        Appointments</a>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>

                @endif
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')


@stop