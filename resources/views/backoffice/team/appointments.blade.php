@extends('layouts.panel')
@section('title')
Scheduled Appointments of {{ $user->name }}
@parent
@stop

@section('content')


<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-10">
                            <h4 class="card-title float-left"> Scheduled Appointments of {{ $user->name }}</h4>
                        </div>
                        <div class="col-md-2 float-right">
                            <a class="btn btn-sm btn-primary float-right" href="{{ route('backoffice.team.index') }}">
                                <i class="align-middle feather_small" data-feather="list"></i>
                                List Staff</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    @if($appointments->count() > 0)
                    <div class="table-responsive-sm">
                        <table class="table">
                            <thead class="thead-header">
                                <tr>
                                    <th>Appointment At</th>
                                    <th>Service</th>
                                    <th>Staff</th>
                                    <th>Customer Name</th>
                                    <th>Customer Email</th>
                                    <th>Customer Phone</th>
                                    <th>Verified</th>
                                    <th>Created At</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($appointments as $app)
                                <tr>
                                    <td>{!! $app->appointment_time !!}</td>
                                    <td>{{ $app->service->name }}</td>
                                    <td>{{ $app->staff->name }}</td>
                                    <td>{{ $app->customer_name }}</td>
                                    <td>{{ $app->customer_email }}</td>
                                    <td>{{ $app->customer_phone }}</td>
                                    <td>{{ $app->is_verified  }}</td>
                                    <td>{{ Carbon\Carbon::parse($app->created_at)->format('d M Y H:i') }}</td>
                                    <td><a class="btn btn-sm btn-primary"
                                            href="{{ route('backoffice.appointments.view', $app->id) }}">
                                            <i class="align-middle feather_small" data-feather="eye"></i>
                                            {{ trans('labels.view') }}</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $appointments->render() }}
                    @else
                    <div class="alert alert-warning">Sorry, no appointments for {{ $user->name }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')


@stop