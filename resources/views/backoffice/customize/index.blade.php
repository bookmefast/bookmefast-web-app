@extends('layouts.panel')
@section('title')
Customize Your Webpage
@parent
@stop
@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/cropper/2.3.4/cropper.css" rel="stylesheet" />
<style>
.preview_image {
    max-width: 100%;
    /* This rule is very important, please do not ignore this! */
}

.img-container,
.img-preview {
    background-color: #ffffff;
    width: 100%;
    text-align: center;
}

.img-container {
    min-height: 313px;
    max-height: 313px;
    margin-bottom: 20px;
}

@media (min-width: 768px) {
    .img-container {
        min-height: 300px;
    }
}
</style>
@stop
@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <strong> Customize Your Page</strong>
                </div>
                <div class="card-body">
                    @include('panel.partials.notifications')
                   <div class="row">
                        <div class="col-sm-10 offset-sm-1">
                            <form autocomplete="off" role="form" enctype="multipart/form-data"
                                action="{{ route('backoffice.account.update') }}" method="POST">
                                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

                                <div class="form-group">
                                    <label for="lesson" class="col-md-4 control-label">Profile Photo or Logo:</label>
                                    <div class="col-md-6">
                                        <div class="profile_image">
                                            <img id="profile_image" style="width: 450px; height: 313px"
                                                src="{{ (isset($company->logo)) ?  asset('storage/company/images/'. $company->id .'/'. $company->logo ) : '/images/No_Image_Available.jpg' }}" />
                                        </div> 
                                        <div class="img-container" style="display:none">
                                            <img id="preview_image"
                                                src="{{ (isset($company->logo)) ?  asset('storage/company/images/'. $company->id .'/'. $company->logo ) : '/images/No_Image_Available.jpg' }}" />
                                        </div>
                                        <div class="docs-buttons" style="text-align:center; display:none">
                                            <button type="button" class="btn btn-primary" data-method="crop"
                                                title="Crop">
                                                <span class="docs-tooltip" data-toggle="tooltip" title="Crop">
                                                    <svg class="align-middle feather_small">
                                                        <use xlink:href="/fonts/feather-sprite.svg#check" />
                                                    </svg>
                                                </span>
                                            </button>
                                            <button type="button" class="btn btn-primary" data-method="clear"
                                                title="Clear">
                                                <span class="docs-tooltip" data-toggle="tooltip" title="Clear">
                                                    <svg class="align-middle feather_small">
                                                        <use xlink:href="/fonts/feather-sprite.svg#minus-circle" />
                                                    </svg>
                                                </span>
                                            </button>
                                            <label class="btn btn-warning btn-upload btn-lg" for="inputImage"
                                                title="Upload image file">
                                                <input type="file" class="sr-only" id="inputImage" name="file"
                                                    accept="image/*">
                                                <span class="docs-tooltip" data-toggle="tooltip" title="Upload photo">
                                                    <svg class="align-middle feather_small">
                                                        <use xlink:href="/fonts/feather-sprite.svg#upload" />
                                                    </svg> Upload
                                                </span>
                                            </label>

                                            <button type="button" class="btn btn-primary"
                                                data-method="getCroppedCanvas">
                                                <span class="docs-tooltip" data-toggle="tooltip" title="Save">
                                                    <svg class="align-middle feather_small">
                                                        <use xlink:href="/fonts/feather-sprite.svg#save" />
                                                    </svg> Save
                                                </span>
                                                </span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <h3>Templates</h3>
                    <form autocomplete="off" role="form" enctype="multipart/form-data"
                        action="{{ route('backoffice.account.template.save') }}" method="POST">
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

                        <div class="form-group template_id {!! $errors->first('template_id', 'has-warning') !!}">
                            <label for="template_id">Change Template</label>
                            {{ Form::select('template_id', $templates, old('template', isset($company) ? $company->template_id : null), ['class' => 'template_id form-control select2', 'id' => 'template_id']) }}
                            {!! $errors->first('template_id', '<span class="help-block">:message</span>') !!}
                        </div>
                        <button type="submit" class="btn btn-info btn-md pull-right">Update</button>
                    </form>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/cropper/2.3.4/cropper.js"></script>
<script src="/js/profile-upload.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.docs-buttons').css('display', 'none');
    $('.profile_image').click(function() {
        $('.img-container').css('display', 'block');
        $('.docs-buttons').css('display', 'block');
        $('.profile_image').hide();
    });
});
</script>
@stop
