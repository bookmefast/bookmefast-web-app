@extends('layouts.panel')
@section('title')
{{ (isset($service)) ? 'Delete a service' : 'Add a Service' }}
@parent
@stop
@section('css')
<link href="{{ mix('css/summernote-lite.css') }}" rel="stylesheet">
@stop
@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header container-fluid">
                    <div class="row">
                        <div class="col-md-8">

                            <strong>{{ (isset($service)) ? 'Delete a service' : 'Add a Service' }}</strong>
                        </div>
                        <div class="col-md-4 ">

                            <a class="btn btn-sm btn-primary float-right"
                                href="{{ route('backoffice.services.index') }}">
                                <i class="align-middle feather_small" data-feather="list"></i>
                                List Services</a>
                        </div>
                    </div>

                </div>
                <div class="card-body">
                    @include('backoffice.partials.notifications')

                    <div class="row">
                        <div class="col-sm-10 offset-sm-1">
                            <form autocomplete="off" role="form"
                                action="{{ (isset($service)) ? route('backoffice.services.destroy', $service->id ) : route('backoffice.services.store') }}"
                                method="POST">
                                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

                                <div class="form-group row row {!! $errors->first('name', 'has-warning') !!}">
                                    <label for="name" class="col-sm-4 col-form-label">{{ trans('labels.name') }}
                                        {{ old('name')  }}</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="name" name="name" autocomplete="off"
                                            value="{{{ old('name', isset($service) ? $service->name : null) }}}" readonly>
                                        {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                                 

                                 
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Service details</label>
                                    <div class="col-sm-8">
                                      There are {{ $service->appointments->count() }} appointments under this service. <br/>
                                      There are {{ $service->team->count() }} team members under this service.<br/>

                                      The related datas will be remained in database but will not be displayed on your site. 
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-primary pull-right">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                   Delete</button>

                            </form>


                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ mix('js/summernote-lite.min.js') }}"></script>

<script>
$(document).ready(function() {
    $('#description').summernote({
        height: 100,
        toolbar: false,
        tabsize: 2
    });
});
</script>
@stop