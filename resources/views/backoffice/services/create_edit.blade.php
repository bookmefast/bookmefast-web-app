@extends('layouts.panel')
@section('title')
{{ (isset($service)) ? 'Update a service' : 'Add a Service' }}
@parent
@stop
@section('css')
<link href="{{ mix('css/summernote-lite.css') }}" rel="stylesheet">
@stop
@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header container-fluid">
                    <div class="row">
                        <div class="col-md-8">

                            <strong>{{ (isset($service)) ? 'Update a service' : 'Add a Service' }}</strong>
                        </div>
                        <div class="col-md-4 ">

                            <a class="btn btn-sm btn-primary float-right"
                                href="{{ route('backoffice.services.index') }}">
                                <i class="align-middle feather_small" data-feather="list"></i>
                                List Services</a>
                        </div>
                    </div>

                </div>
                <div class="card-body">
                    @include('backoffice.partials.notifications')

                    <div class="row">
                        <div class="col-sm-10 offset-sm-1">
                            <form autocomplete="off" role="form"
                                action="{{ (isset($service)) ? route('backoffice.services.update', $service->id ) : route('backoffice.services.store') }}"
                                method="POST">
                                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

                                <div class="form-group row row {!! $errors->first('name', 'has-warning') !!}">
                                    <label for="name" class="col-sm-4 col-form-label">{{ trans('labels.name') }}
                                        {{ old('name')  }}</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="name" name="name" autocomplete="off"
                                            value="{{{ old('name', isset($service) ? $service->name : null) }}}">
                                        {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="form-group row {!! $errors->first('description', 'has-warning') !!}">
                                    <label for="description"
                                        class="col-sm-4 col-form-label">{{ trans('labels.description') }}
                                        </label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control" id="description"
                                            name="description">{{{ old('description', isset($service) ? $service->description : null) }}}</textarea>
                                        {!! $errors->first('descripton', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="form-group row {!! $errors->first('duration', 'has-warning') !!}">
                                    <label for="duration" class="col-sm-4 col-form-label">{{ trans('labels.duration') }}
                                        {{ old('duration')  }}
                                        (mins*)</label>
                                    <div class="col-sm-8">
                                        {{ Form::select('duration', $durations, (isset($service) ? $service->duration : null), ['class'=>'form-control col-sm-3']) }}
                                        {!! $errors->first('duration', '<span class="help-inline">:message</span>') !!}
                                        {!! $errors->first('duration', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="form-group row {!! $errors->first('price', 'has-warning') !!}">
                                    <label for="price" class="col-sm-4 col-form-label">{{ trans('labels.price') }}
                                        {{ old('price')  }}</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control col-sm-4" id="price" name="price"
                                            autocomplete="off"
                                            value="{{{ old('price', isset($service) ? $service->price : null) }}}">
                                        {!! $errors->first('price', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">{{ trans('labels.team_member') }} </label>
                                    <div class="col-sm-8">
                                        @foreach($employees as $user)
                                        <div class="form-check" class="col-sm-4 col-form-label">
                                                @if(isset($service))
                                                <input type="checkbox" class="form-check-input" name="staff[]"
                                                    id="staff{{ $user->id }}" value="{{ $user->id }}"
                                                    {{ (in_array($user->id, $service->team->pluck('id')->toArray()) ? 'checked' : '') }} />
                                                @else
                                                <input type="checkbox" class="form-check-input" name="staff[]"
                                                    id="staff{{ $user->id }}" value="{{ $user->id }}" />
                                                @endif
                                                <label class="form-check-label" for="staff{{ $user->id }}">
                                                {{ $user->name }}</label>
                                        </div>

                                        @endforeach
                                        {!! $errors->first('staff[]', '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-primary pull-right">
                                    <i class="fa fa-save" aria-hidden="true"></i>
                                    {{ trans('labels.save') }}</button>

                            </form>


                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ mix('js/summernote-lite.min.js') }}"></script>

<script>
$(document).ready(function() {
    $('#description').summernote({
        height: 100,
        toolbar: false,
        tabsize: 2
    });
});
</script>
@stop