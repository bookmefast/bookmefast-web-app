@extends('layouts.panel')
@section('title')
Services
@parent
@stop

@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header container-fluid">
                    <div class="row">
                        <div class="col-md-8">
                            <strong>{{ (isset($service)) ? 'Update a service' : 'Add a Service' }}</strong>
                        </div>
                        <div class="col-md-4 ">
                            <a class="btn btn-sm btn-primary float-right"
                                href="{{ route('backoffice.services.create') }}">
                                <i class="align-middle feather_small" data-feather="plus"></i>
                                Add Service</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    @include('backoffice.partials.notifications')

                    @if($services->count() < 1) <div class="alert alert-warning">
                        Sorry there is no service!
                </div>
                @else
                <div class="table-responsive-sm">
                    <table class="table table-striped table-hover">
                        <thead class="thead-header">
                            <tr>
                                <th>Service</th>
                                <th>Duration</th>
                                <th>Fee</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($services as $service)
                            <tr>
                                <td>{{ $service->name }}</td>
                                <td>
                                    <i class="align-middle feather_small" data-feather="clock"></i>
                                    {{ $service->duration }} min.
                                </td>
                                <td>£{{ $service->service_price }}</td>
                                <td>

                                <a class="btn btn-sm btn-primary float-right ml-1"
                                        href="{{ route('backoffice.services.delete', $service->id) }}">
                                        <i class="align-middle feather_small" data-feather="delete"></i>
                                        Delete</a>

                                    <a class="btn btn-sm btn-primary float-right ml-1"
                                        href="{{ route('backoffice.services.edit', $service->id) }}">
                                        <i class="align-middle feather_small" data-feather="edit"></i>
                                        Edit</a>
                                    <a class="btn btn-sm btn-primary float-right ml-1"
                                        href="{{ route('backoffice.services.upload', $service->id) }}">
                                        <i class="fas fa-upload"></i> Upload</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
</div>
@endsection
@section('scripts')


@stop