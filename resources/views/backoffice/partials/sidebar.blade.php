<div class="sidebar">

    <div class="logo">
        <a href="{{ route('backoffice.dashboard') }}" class="simple-text logo-normal text-center">
            BOOKMEFAST
        </a>
    </div>
    <div class="sidebar-wrapper">

        <ul class="nav">
            <li class="">
                <a href="{{ route('backoffice.dashboard') }}">
                    <i class="align-middle" data-feather="circle"></i>
                    <span class="align-middle pl-3">Dashboard</span>
                </a>
            </li>

            @roles(["owner", 'team_member'])
            <li class="">
                <a href="{{ route('backoffice.appointments') }}">
                    <i class="align-middle" data-feather="book"></i>
                    <span class="align-middle pl-3">Appointments</span>
                </a>
            </li>
            {{-- 
            <li class="" style="display: none">
                <a href="{{ route('backoffice.customers.index') }}">
            <i class="align-middle" data-feather="users"></i>
            <span class="align-middle pl-3">Customers</span>
            </a>
            </li>
            --}}

            @endauth

            @roles('owner')

            <li class="">
                <a href="{{ route('backoffice.services.index') }}">
                    <i class="align-middle" data-feather="briefcase"></i>
                    <span class="align-middle pl-3">Appointment Types</span>
                </a>
            </li>
            
            <li class="">
                <a href="{{ route('backoffice.team.index') }}">
                    <i class="align-middle" data-feather="disc"></i>
                    <span class="align-middle pl-3">Team</span>
                </a>
            </li>
            <li class="">
                <a href="{{ route('backoffice.account') }}">
                    <i class="align-middle" data-feather="settings"></i>
                    <span class="align-middle pl-3">Account</span>
                </a>
            </li>
            @endauth 
        </ul>
    </div>