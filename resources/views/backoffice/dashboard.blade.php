@extends('layouts.panel')
@section('title') Dashboard @parent @stop
@section('content')


<div class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"> Today's Appointment</h4>
                </div>
                <div class="card-body">
                    @if($today_appointments->count() < 1) <div class="alert alert-warning">Sorry there is no
                        appointment!</div>
                @else
                <div class="table-responsive-sm">
                    <table class="table">
                        <thead class="">
                            <tr>
                            <th style="width: 25%">Date</th>
                            <th style="width: 55%">Emp.</th>
                            <th style="width: 20%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($today_appointments as $app)
                            <tr>
                                <td>{!! $app->appointment_time_user !!}</td>
                                <td>{{ $app->customer_name }}</td>

                                <td><a class="btn btn-sm btn-primary btn-flat m-b-10 m-l-5"
                                    href="{{ route('backoffice.appointments.view', $app->id) }}">
                                    <i class="align-middle feather_small" data-feather="eye"></i>
                                    {{ trans('labels.view') }}</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                @endif
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title"> Upcoming Appointment<h4>
            </div>
            <div class="card-body">
                @if($upcoming_appointments->count() < 1) <div class="alert alert-warning">Sorry there is no appointment!
            </div>
            @else
            <div class="table-responsive-sm">
                <table class="table">
                    <thead class="">
                        <tr>
                            <th style="width: 25%">Date</th>
                            <th style="width: 55%">Name</th>
                            <th style="width: 20%"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($upcoming_appointments as $app)
                        <tr>
                            <td>{!! $app->appointment_time_user !!}</td>
                            <td>{{ $app->customer_name }}</td>
                            <td><a class="btn btn-sm btn-primary btn-flat m-b-10 m-l-5"
                                    href="{{ route('backoffice.appointments.view', $app->id) }}">
                                    <i class="align-middle feather_small" data-feather="eye"></i>
                                    {{ trans('labels.view') }}</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            @endif
        </div>
    </div>
</div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title"> Pending Appointments<h4>
            </div>
            <div class="card-body">
                @if($unapproved_appointments->count() < 1) <div class="alert alert-warning">You do not have any
                    unapproved appointments!</div>
            @else
            <table class="table table-striped table-hover">
                <thead class="thead-header">
                    <tr>
                        <th style="width:15%">Booked At</th>
                        <th style="width:20%">Service</th>
                        <th style="width:20%">Emp.</th>
                        <th style="width:20%">Name</th>
                        <th style="width:15%">Created At</th>
                        <th style="width:10%"></th>

                    </tr>
                </thead>
                <tbody>
                    @foreach($unapproved_appointments as $app)
                    <tr>
                        <td>{!! $app->appointment_time_user !!}</td>
                        <td>{{ $app->service->name }}</td>
                        <td>{{ $app->staff->name }}</td>
                        <td>{{ $app->customer_name }}</td>
                        <td>{{ Carbon\Carbon::parse($app->created_at)->format('d M Y H:i') }}</td>
                        <td><a class="btn btn-sm btn-primary"
                                href="{{ route('backoffice.appointments.view', $app->id) }}">
                                <i class="align-middle feather_small" data-feather="eye"></i>
                                {{ trans('labels.view') }}</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        @endif
    </div>
</div>
@endsection
@section('scripts')
<script>

</script>
@stop