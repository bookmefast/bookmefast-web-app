@extends('frontoffice.basic.layout')
@section('title') {{ $company->name }} @parent @stop

@section('content')
 
@include('frontoffice.basic.partials.services')

@endsection

@section('scripts') @stop