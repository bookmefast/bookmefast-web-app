@extends('frontoffice.basic.layout')
@section('title') {{ $company->name }} @parent @stop

@section('content')


<div id="thankyou" class="px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center" style="display: none">
    <span class="display-5 text-success">Your appointment had been booked, Thank you</span>
</div>

<div class="row">
    <div class="col-sm-12">
        <form class="form-inline">
            <div class="form-group m-2">
                <label for="start_date" class="p-2">Date</label>
                <input class="form-control col-12" id="selected_date" name="book_date" />
            </div>

            <div class="form-group m-2">
                <label for="timezone" class="p-2">Your time zone </label>
                {{ Form::select('timezone', $timezones, $selected, ['class'=>'form-control timezone col-12']) }}
                {!! $errors->first('timezone', '<span class="help-inline">:message</span>') !!}
            </div>
            <div class="form-group  m-2">
                <button type="button" class="btn btn-primary availability">Show
                    Availability</button>
            </div>
        </form>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="p-3">
            <strong>Staff Name:</strong> {{ $user->name }}<br />
            <strong>Service Name:</strong> {{ $service->name }}<br />
            <p>{{ $service->duration }} minutes
                @if ($service->price > 0)
                / costs £{{ $service->price }}
                @endif
            </p>
        </div>
        <form id="book" action="" method="">
            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
            <input type="hidden" name="user_id" value="{{ $user->id }}" />
            <input type="hidden" name="company_id" value="{{ $company->id }}" />
            <input type="hidden" name="service" value="{{ $service->id }}" />
            <input type="hidden" class="selected_date" name="selected_date" />
            <input type="hidden" class="selected_time" name="selected_time" />
            <div class="loader" style="display: none"></div>
            <ul id="hours" class="list-inline"></ul>
        </form>
    </div>
</div>
 <div class="modal fade" id="clientDetails" tabindex="-1" role="dialog" aria-labelledby="clientModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="clientModalLabel">Confirm the appointment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form>
                    <div class="shadow-sm p-3 mb-5 bg-white rounded">
                        <strong>Staff Name:</strong> {{ $user->name }}<br />
                        <strong>Service Name:</strong> {{ $service->name }}<br />
                        <strong>App. Date:</strong> <span class="selected_date_time"></span> <br />
                    </div>

                    <div class="form-group row">
                        <label for="client_email" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="client_email" value="">
                            <span class="error_email text-danger book_error"></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="client_name" class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="client_name" placeholder="">
                            <span class="error_name text-danger book_error"></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="client_phone" class="col-sm-2 col-form-label">Phone</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="client_phone" placeholder="">
                            <span class="error_phone text-danger book_error"></span>
                        </div>
                    </div>

                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary save_book">Save Appointment</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type='text/javascript'>
var today = new Date();
var tomorrow = new Date();
var apiurl = "{{env('APP_URL', 'http://localhost')}}";

tomorrow.setDate(today.getDate() + 1);

$('#selected_date').datepicker({
    dateFormat: "yy-mm-dd",
    minDate: tomorrow,
    maxDate: "+1m +1w"
});

$('#selected_date').on('change', function() {
    $('.selected_date').val($(this).val());
});

$('.availability').on('click', function() {

    $('.loader').show();
    var user_id = $("#book :input[name='user_id']").val();
    var company_id = $("#book :input[name='company_id']").val();
    var service = $("#book :input[name='service']").val();
    var selected_date = $(".selected_date").val();
    var timezone = $('.timezone').val();
    var req = {
        user_id: user_id,
        service: service,
        company_id: company_id,
        date: selected_date,
        timezone: timezone
    }
    $('#hours').empty();
    var url = `${apiurl}/api/v1/book/check-availability`;
    $.ajax({
        type: "POST",
        dataType: 'json',
        headers: {
            'Authorization': 'Bearer B2ar7L0rj5GZR4yMUe9nlRpjwnWUKkSeCr1QrUuWnHVMTsRYnsGQolr5pTtS',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            'Content-Type': 'application/json'
        },
        url: url,
        data: JSON.stringify(req),
        success: function(data) {

            timeplan = data.timePlan;
            if (timeplan.length < 1) {
                alert('There is no free slot on selected date');
                $('.closed').show();
            } else {
                $('.closed').hide();

                data.timePlan.forEach(function(res, i) {
                    if (res.available == 0) {
                        $('#hours').append(
                            '<li class="list-inline-item m-2"><button type="button" data-slot="' +
                            i + '" data-utc_time="' + res.utc_timezone_start +
                            '" data-time="' + res.start + '" data-showtime="' + res
                            .date_time + ' - (' + res
                            .timezone_start + " - " +
                            res.timezone_end + ')" class="slot' + i +
                            '  book_time btn btn-primary btn-sm">' + res
                            .timezone_start + " - " +
                            res.timezone_end + '</button></li>');
                    } else {
                        $('#hours').append(
                            '<li class="list-inline-item m-2"><button type="button" data-slot="' +
                            i + '" readonly="readonly" disabled class="slot' + i +
                            ' book_time btn-warning btn btn-sm">' + res.timezone_start +
                            " - " +
                            res.timezone_end + '</button></li>');
                    }
                });
                $('.loader').hide();
            }
        },
        error: function(data) {
            alert("Free slots can not be founded, please try again.");
            $('.loader').hide();
        }
    });
})

$(document).on('click', '.book_time', function() {
    var selected_hour = $(this).data('utc_time');
    var selected_slot = $(this).data('slot');
    var showtime = $(this).data('showtime');
    $('.book_time').removeClass('btn-outline-primary').addClass('btn-primary');
    $(this).addClass('btn-outline-primary');
    $('.selected_time').val(selected_hour);

    $('.selected_date_time').text(showtime)
    $('#clientDetails').modal('show')
});

$(document).on('click', '.save_book', function() {
    var selected_hour = $('.selected_time').val();
    var selected_date = $('.selected_date').val();
    var client_email = $('#client_email').val();
    var client_name = $('#client_name').val();
    var client_phone = $('#client_phone').val();
    var timezone = $('.timezone').val();

    var user_id = $("#book :input[name='user_id']").val();
    var company_id = $("#book :input[name='company_id']").val();
    var service = $("#book :input[name='service']").val();

    var req = {
        selected_hour: selected_hour,
        selected_date: selected_date,
        email: client_email,
        name: client_name,
        phone: client_phone,
        user_id: user_id,
        company_id: company_id,
        service: service,
        timezone: timezone
    }

    var url = `${apiurl}/api/v1/book/save`;
    $.ajax({
        type: "POST",
        dataType: 'json',
        headers: {
            'Authorization': 'Basic xxxxxxxxxxxxx',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            'Content-Type': 'application/json'
        },
        url: url,
        data: JSON.stringify(req),
        success: function(data) {
            $('.book_error').html('')
            $('#clientDetails').modal('hide')
            $('#book').hide();
            $('#thankyou').show();
        },
        error: function(data) {
            $('.book_error').html('')
            errors = data.responseJSON;
            var err = errors.errors
            for (key in err) {
                row = err[key]
                $('.error_' + key).html(row[0])
            }
        }
    });
});
</script>
@stop