@extends('frontoffice.basic.layout')
@section('title') {{ $user->name }} - {{ $company->name }} @parent @stop

@section('content')

<div class="row">
    <div class="col-12 col-md-6 col-lg-4">
        @if($user->image)
        <img src="{{ Storage::url('uploads/users/thump_'.$user->image)  }}" class="img-fluid m-2" />
        @else
        <img src="{{ asset('images/not-available.png')}}" class="img-fluid m-2" />
        @endif
    </div>
    <div class="col-12 col-md-6 col-lg-8">
        <div class="px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
            <h3 class="display-5">{{ $user->name }}</h3>
        </div>
        <p class="">{!! $company->description !!}</p>
    </div>
</div>

<h4 class="text-center">Choose your service:</h4>

@include('frontoffice.basic.partials.services')
@endsection

@section('scripts') @stop