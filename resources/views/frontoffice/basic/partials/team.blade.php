@if ($company->team->count() > 1)
<section class="ilitems padding-lg">
    <h3>Team:</h3>
    <ul class="row">
        @foreach($company->team as $user)

        @if ($user->services()->count() > 0)
        <li class="col-12 col-md-6 col-lg-4">
            <div class="cnt-block" style="height: 349px;">
                <h3><a
                        href="{{ route('frontoffice.team.member.view', [  $user->slug]) }}">{{ $user->name }}</a>
                </h3>
                @if(isset($user->slug) && isset($service->slug))
                <a href="{{ route('frontoffice.book.create', [ $user->slug, $service->slug]) }}">
                    @if($user->image)
                    <img src="{{ Storage::url('uploads/users/thump_'.$user->image)  }}" class="img-fluid m-2" />
                    @else
                    <img src="{{ asset('images/not-available.png')}}" class="img-fluid m-2" />
                    @endif
                </a>

                <a class="btn btn-lg btn-info btn-block"
                    href="{{ route('frontoffice.book.create', [ $user->slug, $service->slug]) }}">Book
                    Now</a>
                @else
                <a href="{{ route('frontoffice.team.member.view', [  $user->slug]) }}">
                    @if($user->image)
                    <img src="{{ Storage::url('uploads/users/thump_'.$user->image)  }}" class="img-fluid m-2" />
                    @else
                    <img src="{{ asset('images/not-available.png')}}" class="img-fluid m-2" />
                    @endif
                    @endif
            </div>
        </li>
        @endif
        @endforeach
    </ul>
</section>
@endif