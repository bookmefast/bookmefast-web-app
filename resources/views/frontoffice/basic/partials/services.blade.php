@if ($company->services->count() > 0)

@if ($company->services->count() == 1)
@php
$service = $company->services->first();
@endphp


<section class="ilitems padding-lg">
    <ul class="row">
        <li class="col-12 col-md-12 col-lg-12">
            <div class="cnt-block" style="height: 389px;">
                <h3><a
                        href="{{ route('frontoffice.service.view', [$service->slug]) }}">{{ $service->name }}</a>
                </h3>

                @if ($service->image)
                <a href="{{ route('frontoffice.service.view', [$service->slug]) }}">
                    <img src="{{ Storage::url('uploads/services/thump_'.$service->image )  }}"
                        class="img-fluid m-2" /></a>
                @else
                <a href="{{ route('frontoffice.service.view', [$service->slug]) }}">
                    <img src="{{ asset('images/not-available.png')}}" class="img-fluid m-2" /></a>


                @endif
                <p>{{ $service->duration }} minutes</p>
                @if ($service->price > 0)
                <p>costs £{{ $service->price }}</p>
                @endif
                @if(isset($user->slug) && isset($service->slug))
                <a class="btn btn-sm btn-info btn-block"
                    href="{{ route('frontoffice.book.create', [$user->slug, $service->slug]) }}">Book
                    Now</a>
                @endif
            </div>
        </li>
    </ul>
</section>

@else
<section class="ilitems padding-lg">
    <h3>Services:</h3>
    <ul class="row">
        @foreach($company->services as $service)
        <li class="col-12 col-md-6 col-lg-4">
            <div class="cnt-block" style="height: 389px;">
                <h3><a
                        href="{{ route('frontoffice.service.view', [$service->slug]) }}">{{ $service->name }}</a>
                </h3>

                @if ($service->image)
                <a href="{{ route('frontoffice.service.view', [$service->slug]) }}">
                    <img src="{{ Storage::url('uploads/services/thump_'.$service->image )  }}"
                        class="img-fluid m-2" /></a>
                @else
                <a href="{{ route('frontoffice.service.view', [$service->slug]) }}">
                    <img src="{{ asset('images/not-available.png')}}" class="img-fluid m-2" /></a>


                @endif
                <p>{{ $service->duration }} minutes</p>
                @if ($service->price > 0)
                <p>costs £{{ $service->price }}</p>
                @endif
                @if(isset($user->slug) && isset($service->slug))
                <a class="btn btn-md btn-info btn-block"
                    href="{{ route('frontoffice.book.create', [$user->slug, $service->slug]) }}">Book
                    Now</a>
                @endif
            </div>
        </li>
        @endforeach
    </ul>
</section>
@endif
@endif