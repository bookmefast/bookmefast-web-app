<header>
    <div class="px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <h1 class="display-4">{{ $company->name }}</h1>
        <p class="">{!! $company->description !!}</p>
        <p class="lead">{{ $company->address_info }} <br/>
        @if($company->phone)
        Phone:{{ $company->phone }}
        @endif
        </p>
    </div>
</header>
