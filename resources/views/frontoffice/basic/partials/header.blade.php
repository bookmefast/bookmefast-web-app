<header>
    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
        <h5 class="my-0 mr-md-auto font-weight-normal"><a
                href="{{ route('frontoffice.home') }}">{{ $company->name }}</a></h5>
        <nav class="my-2 my-md-0 mr-md-3">
        @if ($company->team->count() > 1)
            <a class="p-2 text-dark" href="{{ route('frontoffice.team.index') }}">Team</a>
            @endif
            <a class="p-2 text-dark" href="{{ route('frontoffice.services.index') }}">Services</a>
        </nav>
    </div>
</header>