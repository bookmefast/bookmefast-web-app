@extends('frontoffice.basic.layout')
@section('title'){{ $service->name }} - {{ $company->name }} @parent @stop

@section('content')


<div class="row">
    <div class="col-12 col-md-6 col-lg-4">
        @if($service->image)
        <img src="{{ Storage::url('uploads/services/thump_'.$service->image)  }}" class="img-fluid m-2" />
        @else
        <img src="{{ asset('images/not-available.png')}}" class="img-fluid m-2" />
        @endif
    </div>
    <div class="col-12 col-md-6 col-lg-8">
        <div class="px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
            <h3 class="display-5">{{ $service->name }}</h3>
        </div>
        <p class="">{!! $service->description !!}</p>
        <p>{{ $service->duration }} minutes
            @if ($service->price > 0)
            / costs £{{ $service->price }}
            @endif
        </p>
    </div>
</div>


@if ($company->team->count() > 1)
<h4>Choose your employee:</h4>
@include('frontoffice.basic.partials.team')
@else

<div class="px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
    <a class="btn btn-lg btn-primary "
        href="{{ route('frontoffice.book.create', [$company->users()->first()->slug, $service->slug]) }}">Book
        Now</a>
</div>
@endif

@endsection

@section('scripts') @stop