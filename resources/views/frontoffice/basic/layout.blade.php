<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="/images/favicon.png" />
    <title>
        @section('title')
        | BookMeFast
        @show
    </title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
        name='viewport' />

    <link href="{{ mix('css/theme.css') }}" rel="stylesheet">

    @yield('css')

</head>

<body>
    @include('frontoffice.basic.partials.header')
    <div class="container bg-white">
        @yield('content')
        <footer class="pt-4 my-md-5 pt-md-5 border-top">
            <div class="row">
                <div class="col-12 col-md">
                    <p class="float-right"> Booking service by <a href="https://bookmefast.co.uk"
                            title="Servicing by BookMe Fast ">BookMaFast</a> | Copyright &copy;2020 - All rights
                        reserved
                    </p>
                </div>
            </div>
        </footer> 
            @include('cookieConsent::index')
     
    </div>
    <script src="{{ mix('js/app.js') }}"></script>
    @yield('scripts')
</body>

</html>