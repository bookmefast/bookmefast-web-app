<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="/images/favicon.png" />
    <title>
        @section('title')
        | Bookmefast
        @show
    </title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
        name='viewport' />

    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link href="{{ mix('css/all.css') }}" rel="stylesheet">

    @yield('css')
</head>

<body class="">
    <div class="wrapper">
  

        <div class="panel-header panel-header-sm">
        </div>
        @yield('content')
        <footer class="footer">
            <div class="container-fluid">
                <nav>
                    <ul>

                    </ul>
                </nav>
                <div class="copyright ">
                    <p class="text-center">&copy; 2020 <a href="http://www.bookmefast.co.uk">BookMeFAST</a> All Rights Reserved
                    </p>
                </div>
            </div>
        </footer>
    </div>
    </div>

    <script src="{{ mix('js/app.js') }}"></script>

    @yield('scripts')
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-136232004-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-136232004-1');
    </script>


</body>

</html>