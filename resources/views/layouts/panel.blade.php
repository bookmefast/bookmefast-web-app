<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <!-- CSRF Token -->

    <link rel="icon" type="image/png" href="/images/favicon.png" />
    <title>
        @section('title')
        | Bookmefast
        @show
    </title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
        name='viewport' />

    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link href="{{ mix('css/all.css') }}" rel="stylesheet">

    @yield('css')

</head>

<body class="">
    <div class="wrapper">
        @include('backoffice.partials.sidebar')
    </div>
    <div class="main-panel">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-absolute bg-white text-dark fixed-top">
            <div class="container-fluid">
                <div class="navbar-wrapper">
                    <div class="navbar-toggle">
                        <button type="button" class="navbar-toggler">
                            <span class="navbar-toggler-bar bar1"></span>
                            <span class="navbar-toggler-bar bar2"></span>
                            <span class="navbar-toggler-bar bar3"></span>
                        </button>
                    </div>
                    <a class="navbar-brand" href="">

                    </a>

                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation"
                    aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navigation">

                    <ul class="navbar-nav">
          
         
                    <li class="nav-item">
                            <a class="nav-link" href="{{ route('backoffice.appointments.create') }}">
                            <i data-feather="plus" class="align-middle"></i>
                            Create Appointment</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('backoffice.profile') }}">
                            <i data-feather="user" class="align-middle"></i>
                            Profile</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                                <i data-feather="log-out" class="align-middle"></i>
                                <p>
                                    <span class="d-lg-none d-md-block">{{__('panel.logout')}}</span>
                                </p>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>

                    </ul>
                </div>
            </div>
        </nav>
        <!-- End Navbar -->
        <div class="panel-header panel-header-sm">
        </div>
        @yield('content')
        <footer class="footer">
            <div class="container-fluid">
                <nav>
                    <ul>

                    </ul>
                </nav>
                <div class="copyright">
                    <p>&copy; 2020 <a href="https://www.bookmefast.co.uk">BookMeFAST</a> All Rights Reserved
                    </p>
                </div>
            </div>
        </footer>
    </div>
    </div>
    <script src="{{ mix('js/app.js') }}"></script>
    @yield('scripts')
</body>
</html>