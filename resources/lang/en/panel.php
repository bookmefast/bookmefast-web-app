<?php

return [
    'dashboard' => 'Dashboard',
    'calendar' => 'Calendar',
    'reports' => 'Reports',
    'account' => 'Account',
    'name' => 'Name',
    'email' => 'Email',
    'password' => 'Password',
    'password_confirmation' => 'Password Confirmation',
    'save'  => 'Save',
    'url'   => 'URL',
    'start_date'    => 'Start Date',
    'end_date'  => 'End Date',
    'status'    => 'Status',
    'last_check'    => 'Last Check',
    'map_type'  => 'Map Type',
    'monitor' => 'Monitoring',
    'logout'    => 'Logout',
    'form' => [
        'success'   => 'Form has been save',
        'error'   => 'Failed to save the form',
    ]
];