<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'name' => 'Name',
    'save' => 'Save',
    'view' => 'View',
    'description'   => 'Description',
    'form' => [
        'success'   => 'Record saved!',
        'error'   => 'Record not saved!',        
    ],

    'password'  => 'Password',
    'password_confirmation'  => 'Password confirmation',
    'email' => 'Email',
    'language' => 'Language',
    'address' => 'Address',
    'postcode'  => 'Postcode',
    'timezone'  => 'Timezone',
    'start_time'    => 'Starts At',
    'end_time'    => 'Ends At',    
    'Sunday'       => 'Sunday',
    'Monday'    => 'Monday', 
    'Tuesday'   => 'Tuesday',
    'Wednesday' => 'Wednesday',
    'Thursday'  => 'Thursday',
    'Friday'    => 'Friday',
    'Saturday'  => 'Saturday',
    'countries' => 'Countries',
    'cities'    => 'Cities',
    'phone'     => 'Phone',
    'website' => 'Website',
    'staff' => 'Staff',
    'duration'  => 'Duration',
    'price' => 'Price',
    'service_staff_name'    => 'User Name',
    'city'  => 'City',
    'service'   => 'Service',
    'auto_renew'    => 'Auto Renew',
    'team_member'   => 'Team Member',
    'membership_plan'   => "Membership Plan",
    'user'  => 'User',

    'appointment' => [
        'saved' => 'Your appointment saved. We have send you an confirmation email for this appointment. Please open the email and confirmation the appointment!',
    ]
];
