const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


    mix.js('resources/js/app.js', 'public/js')
        .sass('resources/sass/app.scss', 'public/css')
        .sass('resources/sass/theme.scss', 'public/css')  
        .sass('resources/sass/theme-basic.scss', 'public/css')  
    .combine([
        'resources/css/admin.css',
        'resources/css/styles.css'
    ], 'public/css/all.css', __dirname);


    mix.copy('resources/js/summernote-lite.min.js', 'public/js');
    mix.copy('resources/css/summernote-lite.css', 'public/css');