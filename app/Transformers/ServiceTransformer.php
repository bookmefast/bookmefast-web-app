<?php

namespace App\Transformers;

use App\Models\Service;
use League\Fractal\TransformerAbstract;

class ServiceTransformer extends TransformerAbstract
{
    public function transform(Service $service)
    {
        return [
            'id' => (int)$service->id,
            'name' => $service->name,
            'duration' => $service->duration,
            'price' => $service->price
        ];
    }
}
