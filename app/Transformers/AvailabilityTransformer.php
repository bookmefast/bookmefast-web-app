<?php
namespace App\Transformers;

 
 
use League\Fractal\TransformerAbstract;

class AvailabilityTransformer extends TransformerAbstract
{
    public function transform(Service $service)
    {
        return [
            'id'        => (int)$service->id,
            'name'     =>  $service->name,
            'url'     =>  $service->url,
            'map_name'     =>  $service->map_name,
            'map_source'     =>  $service->map_source,
            'last_status'   => $service->last_status,
            'last_check'   => $service->last_check,
        ];
    }
}