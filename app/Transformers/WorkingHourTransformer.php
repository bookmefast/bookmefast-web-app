<?php

namespace App\Transformers;

use App\Models\TimePlan;
use League\Fractal\TransformerAbstract;

class WorkingHourTransformer extends TransformerAbstract
{
    public function transform(TimePlan $timeplan)
    {
        return [
            'id' => (int)$timeplan->id,
            'day' => $timeplan->day,
            'start_time' => $timeplan->start_time,
            'end_time' => $timeplan->end_time,
        ];
    }
}
