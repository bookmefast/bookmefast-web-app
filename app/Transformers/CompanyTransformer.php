<?php

namespace App\Transformers;

use App\Models\Company;
use League\Fractal\TransformerAbstract;

class CompanyTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'users', 'services'
    ];

    public function transform(Company $company)
    {
        return [
            'id' => (int)$company->id,
            'name' => $company->name,
            'description' => $company->description,
            'address_info' => $company->address_info,
            'phone' => $company->phone,
        ];
    }

    public function includeServices(Company $company)
    {
        $services = $company->services;

        return $this->collection($services, new ServiceTransformer);
    }    

    public function includeUsers(Company $company)
    {
        $users = $company->users;

        return $this->collection($users, new UserTransformer);
    }    

}



