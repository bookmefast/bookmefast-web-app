<?php

namespace App\Policies;

use Core\Models\Appointment;
use Modules\Core\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Auth;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the post.
     *
     * @param  \Core\Models\User  $user
     * @param  \App\Post  $post
     * @return mixed
     */
    public function view(User $user)
    {
        $auth = Auth::user();
        if ($auth->hasRole("administrator")) {
            return true;
        }

        $company = $user->company;
        if ($company->id === $auth->company_id) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create posts.
     *
     * @param  \Core\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        $company = $user->company;
        if ($company->id == $appointment->company_id) {
            if (!$user->hasRole("administrator")) {
                return $user->id == $appointment->user_id;
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * Determine whether the user can update the post.
     *
     * @param  \Core\Models\User  $user
     * @param  \App\Post  $post
     * @return mixed
     */
    public function update(User $user, Appointment $appointment)
    {
        $company = $user->company;
        if ($company->id == $appointment->company_id) {
            if (!$user->hasRole("administrator")) {
                return $user->id == $appointment->user_id;
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * Determine whether the user can delete the post.
     *
     * @param  \Core\Models\User  $user
     * @param  \App\Post  $post
     * @return mixed
     */
    public function delete(User $user, Appointment $appointment)
    {
        $company = $user->company;
        if ($company->id == $appointment->company_id) {
            if (!$user->hasRole("administrator")) {
                return $user->id == $appointment->user_id;
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * Determine whether the user can restore the post.
     *
     * @param  \Core\Models\User  $user
     * @param  \App\Post  $post
     * @return mixed
     */
    public function restore(User $user, Appointment $appointment)
    {
        $company = $user->company;
        if ($company->id == $appointment->company_id) {
            if (!$user->hasRole("administrator")) {
                return $user->id == $appointment->user_id;
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * Determine whether the user can permanently delete the post.
     *
     * @param  \Core\Models\User  $user
     * @param  \App\Post  $post
     * @return mixed
     */
    public function forceDelete(User $user, Appointment $appointment)
    {
        $company = $user->company;
        if ($company->id == $appointment->company_id) {
            if (!$user->hasRole("administrator")) {
                return $user->id == $appointment->user_id;
            } else {
                return false;
            }
        }
        return false;
    }
}