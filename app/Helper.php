<?php

namespace App;

use Illuminate\Http\UploadedFile;
use Image;
use Storage;

class Helper {

    public static function convert($date, $to, $from, $format = 'Y-m-d H:i')
    {
        $dd = new \DateTime($date, new \DateTimeZone($from));
        $dd->setTimezone(new \DateTimeZone($to));
        return $dd->format($format);
    }

    static function show_time($date, $timezone = 'UTC')
    {
        $dd = new \DateTime($date, new \DateTimeZone('UTC'));
        $dd->setTimezone(new \DateTimeZone($timezone));
        return $dd->format('d M Y H:i');
    }
 
    static function show_utc_date($date, $timezone = 'UTC')
    {
        try{
          $dd = new \DateTime($date, new \DateTimeZone('UTC'));
          $dd->setTimezone(new \DateTimeZone($timezone));
        }catch(Exception $ex){
          throw new ErrorException();
        }finally{
          return $dd->format('d M Y H:s');
        }
    }

    static function create_utc_time($date, $timezone = 'UTC', $format='Y-m-d H:i')
    {
        $dd = Carbon::createFromFormat($format, $date, $timezone);
        return $dd->setTimezone('UTC');
    }

    static function tax($price, $tax)
    {
        return $price . " " . $tax;
    }

    static function amount($price, $quantity, $product_tax)
    {
        return ($price * (($product_tax / 100) + 1)) * $quantity;
    }

    static function currency($price, Currency $currency)
    {
        $currency_rate = $currency->currency_rate;
        $amount = ($currency_rate) ? ($price * $currency_rate) : $price;
        return self::format_currency($amount, $currency);
    }

    static function format_currency($price, Currency $currency)
    {
        if ($currency->symbol_align == 'left') {
            return sprintf(" %s %s ", $currency->symbol, $price);
        } else {
            return sprintf(" %s %s ", $price, $currency->symbol);
        }
    }

    public function upload(UploadedFile $uploadedFile, $folder = null, $disk = 'public', $name)
    {
        $uploadedFile->storeAs($folder, $name, $disk);
 
        $filepath = Storage::disk($disk)->path($folder . $name );
        $img = Image::make($filepath);
        
        $img->resize(300, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->stream(); 
        Storage::disk($disk)->put($folder .'thump_'.$name , $img);
        return true;
    }
}