<?php

namespace App\Mail;

use App\Models\Appointment;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Helper;

class AppointmentReceivedToStaff extends Mailable
{
    use Queueable, SerializesModels;

    protected $appointment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Appointment $appointment)
    {
        $this->appointment = $appointment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $appointment = $this->appointment;
        
         $this->withSwiftMessage(function ($message) use($appointment){
            $message->to = $appointment->staff->email;
            $message->from = env('MAIL_FROM');
            $message->company = $appointment->company->name;  
            $message->email_type = 'New Appointment to Staff';          
        });  

        $timezone = $appointment->staff->timezone;

        $start_time = Helper::convert($appointment->start_time, $timezone, 'UTC', 'l d M H:i');
        $end_time = Helper::convert($appointment->end_time, $timezone, 'UTC', 'l d M H:i');
        
        return $this->view('emails.appointment_to_staff')
                    ->subject('You have received a new appointment')
                    ->with(
                        [
                            'appointment' => $appointment, 
                            'start_time' => $start_time, 
                            'end_time' => $end_time
                            ]
                        );
    }
}