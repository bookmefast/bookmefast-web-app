<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\User;


class WelcomeMail extends Mailable
{
    use Queueable, SerializesModels;

protected $user;
 

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.welcome_mail')
            ->subject('Welcome to Bookmefast!')->with([
                                'user' => $this->user
                                ]);
    }
}