<?php

namespace App\Console\Commands;

use App\Models\Appointment;
use App\Models\Company;
use App\Models\Service;
use Faker\Factory as Faker;
use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Helper;

class CreateSample extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:samples {company} {count}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create sample data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        return;
        $timezones = [];
        $tzlist = \DateTimeZone::listIdentifiers(\DateTimeZone::ALL);
        foreach ($tzlist as $item) {
            $timezones[] = $item;
        }

        for ($i = 0; $i < $this->argument('count'); $i++) {
            $faker = Faker::create();
            $company = Company::find($this->argument('company'));
            $services = $company->services;
            $service = $services->random();
            $users = $service->team;
            $user = $users->random();

            $timezone = $timezones[rand(0, count($timezones)-1)];


            $fdate = $faker->dateTimeBetween($startDate = '-1 years', $endDate = '+6 months');

            $selected_date = $fdate->format("Y-m-d");  
            $selected_hour = $this->check_availability($service, $user, $faker, $selected_date, $timezone);
            $hour = $selected_hour['start'];
  
            if ($selected_hour['available'] == 1){
                continue;
            }
 
            $start_time = \Carbon\Carbon::create(substr($selected_date, 0, 4), substr($selected_date, 5, 2), substr($selected_date, 8, 2), substr($hour, 0, 2), substr($hour, 3, 2));
            $end_time = \Carbon\Carbon::create(substr($selected_date, 0, 4), substr($selected_date, 5, 2), substr($selected_date, 8, 2), substr($hour, 0, 2), substr($hour, 3, 2));
            $end_time = $end_time->addMinutes($service->duration);

            echo $i . ' - '. $service->name . "\t";
            echo $user->name . "\t"; 
            echo $start_time . ' - '. $end_time . "\n";  
 

            $appointment = new Appointment;
            $appointment->service_id = $service->id;
            $appointment->user_id = $user->id;
            $appointment->company_id = $company->id;
            $appointment->customer_name = $faker->name;
            $appointment->customer_email = $faker->unique()->safeEmail;
            $appointment->customer_phone = $faker->phoneNumber;
            $appointment->start_time = $start_time;
            $appointment->end_time = $end_time;
            $appointment->timezone = $timezone;
            $appointment->approved = rand(0,1);
            $appointment->created_at = $start_time->subDays(rand(0, 30));
            $appointment->save();  
 
            echo "\n ============================== \n";
        }
    }

    protected function check_availability($service, $user, $faker, $dt, $timezone)
    {
        $date = Carbon::create(substr($dt, 0, 4), substr($dt, 5, 2), substr($dt, 8, 2));
        $duration = $service->duration;

        $day = $date->format('l');
        $workingHours = $user->working_hours()->where('day', $day)->first();
        
        $timePlan = [];
        $workingHourArray = [];
        if ($workingHours) {
            $start_workDay = \Carbon\Carbon::create(substr($dt, 0, 4), substr($dt, 5, 2), substr($dt, 8, 2), substr($workingHours->start_time, 0, 2), substr($workingHours->start_time, 3, 2), 'UTC');
            $end_workDay = \Carbon\Carbon::create(substr($dt, 0, 4), substr($dt, 5, 2), substr($dt, 8, 2), substr($workingHours->end_time, 0, 2), substr($workingHours->end_time, 3, 2), 'UTC');
             
            while ($start_workDay < $end_workDay) {
                $start = clone $start_workDay;
                $start_workDay->addMinutes($duration);

                $timePlan[] = [
                    'start' => $start->format('H:i'),
                    'end' => $start_workDay->format('H:i'),
                    'timezone_start' => Helper::convert($start, $timezone, 'UTC', 'H:i'),
                    'timezone_end' => Helper::convert($start_workDay, $timezone, 'UTC', 'H:i'),
                    'date_time' => Helper::convert($start, $timezone, 'UTC', 'd M Y'),
                    'available' => 0,
                ];
            }

            $appointments = $service->appointments()
                ->where('user_id', $user->id)
                ->whereDate('start_time', $date)
                ->whereRaw('(approved = 1 OR (approved = 0 and (TIMESTAMPDIFF(MINUTE,created_at, NOW())) < 60)) ')
                ->get();

            foreach ($appointments as $appointment) {
                $start = array_search($appointment->start_time->format('H:i'), array_column($timePlan, 'start'));
                $end = array_search($appointment->end_time->format('H:i'), array_column($timePlan, 'end'));

                for ($i = $start; $i <= $end; $i++) {
                    $timePlan[$i]['available'] = 1;
                }
            } 
        }
 
        $rnd = rand(0, count($timePlan) - 1);
        return $timePlan[$rnd];
    }
}
