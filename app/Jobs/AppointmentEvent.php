<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Appointment;
use DB;

class AppointmentEvent implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $appointment;
    protected $event;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Appointment $appointment, string $event)
    {
        $this->appointment = $appointment; 
        $this->event = $event;         
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::table('appointment_histories')->insert(
            ['appointment_id' => $this->appointment->id, 'event' => $this->event, 'created_at' => date('Y-m-d H:i:s')]
        );
    }
}
