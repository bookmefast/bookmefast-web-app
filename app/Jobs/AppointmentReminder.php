<?php

namespace App\Jobs;

use App\Mail\AppointmentReceivedToCustomer;
use App\Models\Appointment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AppointmentReminder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        activity()->log('Appointment reminder started');
        $appointments = Appointment::whereRaw('approved = 1 and (TIMESTAMPDIFF(HOUR,NOW(),start_time)) in (2, 24)')->get();
        foreach ($appointments as $appointment) {
            activity()
                ->performedOn($appointment)
                ->log('Appointment reminder had been sent');

            Mail::to($appointment->customer_email)
                ->queue(new AppointmentReceivedToCustomer($appointment));
        }
        activity()->log('Appointment reminder ended');
    }
}
