<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->model('service', 'App\Models\Service');
        $this->model('customer', 'App\Models\Customer');
        $this->model('team', 'App\Models\User');
        $this->model('appointment', 'App\Models\Appointment');
        $this->model('company', 'App\Models\Company');
        Route::bind('order', function ($order_id) {
            $order = Order::find($order_id);
            $user = Auth::user();
            if ($user->hasRole('administrator')){
                return $order;
            }
            if ($order->company_id == $user->company_id) {
                return $order;
            }

            return abort(404);
        });

        Route::bind('serviceSlug', function ($value) {
            return \App\Models\Service::where('slug', $value)->first() ?? abort(403);
        });
       
        Route::bind('userSlug', function ($value) {
            return \App\Models\User::where('slug', $value)->first() ?? abort(404);
        });

        Route::bind('companySlug', function ($value) {
            return \App\Models\Company::where('slug', $value)->first() ?? abort(404);
        });

        Route::bind('account', function ($value) {
            return \App\Models\Company::where('slug', $value)->first() ?? abort(404);
        });        

        Route::model('country', Country::class);
        Route::model('division', Division::class);
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}