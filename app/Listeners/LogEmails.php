<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Events\MessageSending;
use Illuminate\Mail\Events\MessageSent;
use DB;

class LogEmails implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        DB::table('email_audits')->insert(
            [
                'message' => $event->message->getBody(),
                'to' => (empty($event->message->to) ? null : $event->message->to),
                'from' =>  (empty($event->message->from) ? null : $event->message->from),
                'company' => (empty($event->message->company) ? null : $event->message->company),
                'email_type' =>(empty($event->message->email_type) ? null : $event->message->email_type),
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                ]
        );
    }
}