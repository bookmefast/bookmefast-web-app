<?php

namespace App\Http\Controllers\BackOffice;

use App\Models\Appointment;
use Illuminate\Http\Request;
use App\Mail\AppointmentApproved; 
use App\Mail\AppointmentReminder;
use App\Mail\AppointmentCanceled;
use Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Auth;
use Spatie\Activitylog\Models\Activity;

class AppointmentsController extends Controller
{
    public function __construct()
    {
 
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $time = (isset($request->time) ? $request->time : '');
        $approved = (isset($request->approved) ? $request->approved : 'all');
        $today = Carbon::now();
        $appointments = Auth::user()
            ->company
            ->appointments()
            ->where(function ($query) use ($user){
                if (!$user->hasRole('owner')){
                    $query->where('user_id', $user->id);
                }
            })->with('staff:id,name')->with('service:id,name')           
            ->where(function ($query) use ($today, $time) {
                if ($time === 'next') {
                    $query->whereDate('start_time', '>=', $today);
                } elseif ($time === 'previous') {
                    $query->whereDate('start_time', '<=', $today);
                } else {

                }
            })
            ->where(function ($query) use ($approved) {
                if ($approved != 'all'){
                    if ($approved == 'approved') {
                        $query->where('approved', '=', 1);
                    } elseif ($approved == 'pending') {
                        $query->where('approved', '=', null);
                    } else {
    
                    }
                }
            })
            ->orderBy('start_time', 'desc')->paginate();

        return view('backoffice.appointments.index', compact('appointments', 'approved', 'time'));
    }
 
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Appointment $appointment)
    {
        $today = Carbon::now();
        $user = Auth::user();

        if (!$user->hasRole('owner') && $appointment->user_id != $user->id) {
            activity()
            ->causedBy($user)
            ->performedOn($appointment)
            ->log('NotAllowed');        

            return redirect(route('backoffice.appointments'))->with('error', trans('You can not access this appointment!'));        
        }

        $activities = Activity::forSubject($appointment)->get();
        
        activity()
            ->causedBy($user)
            ->performedOn($appointment)
            ->log('Viewed');        

        return view('backoffice.appointments.view', compact('appointment', 'today', 'activities'));
    }

    public function create()
    {
        $timezones = ['' => ' - - - - - - '];
        $tzlist = \DateTimeZone::listIdentifiers(\DateTimeZone::ALL);
        foreach($tzlist as $item){
            $timezones[$item] = $item;
        }        
        $selected = "Europe/London";

        $user = Auth::user();
        $serviceData = $user->company->services()->orderBy('created_at',  'desc')->get();
        $services[0] = " - - - - - ";
        foreach ($serviceData as $service) {
            $services[$service->id] = $service->name;
        }

        $company = $user->company;
        return view('backoffice.appointments.create', compact('timezones', 'selected', 'services', 'user', 'company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Appointment $appointment)
    {
        $serviceData = Auth::user()->company->services()->orderBy('created_at',  'desc')->get();
        $company = Auth::user()->company;
        $users = $appointment->service->team()->pluck('name', 'id');
        $services[0] = " - - - - - ";
        foreach ($serviceData as $service) {
            $services[$service->id] = $service->name;
        }

        $timezones = ['' => ' - - - - - - '];
        $tzlist = \DateTimeZone::listIdentifiers(\DateTimeZone::ALL);
        foreach($tzlist as $item){
            $timezones[$item] = $item;
        }        
        $selected = "Europe/London";

        return view('backoffice.appointments.edit', compact('appointment', 'services', 'users', 'timezones', 'selected', 'company'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cancel(Appointment $appointment)
    {
        $user = Auth::user();

        if (!$user->hasRole('owner') && $appointment->user_id != $user->id) {
            activity()
            ->causedBy($user)
            ->performedOn($appointment)
            ->log('NotAllowed');        
            return redirect(route('backoffice.appointments'))->with('error', trans('You can not access this appointment!'));        
        }

        activity()
            ->causedBy($user)
            ->performedOn($appointment)
            ->log('Cancelled');

        $appointment->delete();
                
        Mail::to($appointment->customer_email)->queue(new AppointmentCanceled($appointment));
        return redirect(route('backoffice.appointments'))->with('success', trans('Appointment has been deleted!'));        
    }

    public function reminder(Appointment $appointment)
    {
        
        $user = Auth::user();
        if (!$user->hasRole('owner') && $appointment->user_id != $user->id) {
            activity()
            ->causedBy($user)
            ->performedOn($appointment)
            ->log('NotAllowed');        
            return redirect(route('backoffice.appointments'))->with('error', trans('You can not access this appointment!'));        
        }

        Mail::to($appointment->customer)->queue(new AppointmentReminder($appointment));
        return redirect(route('backoffice.appointments.view',  $appointment->id))->with('success', 'Appointment reminder email has been sent!');
    }

    public function approve(Appointment $appointment)
    {
        $user = Auth::user();
        if (!$user->hasRole('owner') && $appointment->user_id != $user->id) {
            activity()
            ->causedBy($user)
            ->performedOn($appointment)
            ->log('NotAllowed');        
            return redirect(route('backoffice.appointments'))->with('error', trans('You can not access this appointment!'));        
        }

        $appointment->approved = 1;
 
        if ($appointment->save()) {
            Mail::to($appointment->customer_email)
                    ->queue(new AppointmentApproved($appointment));
            return redirect(route('backoffice.appointments.view',  $appointment->id))->with('success', trans('Appointment has been approved!'));
        }
        return redirect(route('backoffice.appointments.view',  $appointment->id))->with('error', 'An error occured while verifiying this appointment!')->withInput();
    }
}