<?php

namespace App\Http\Controllers\BackOffice;

use App\Http\Requests\StaffRequest;
use App\Http\Requests\StaffUpdateRequest;
use App\Http\Requests\PhotoUpdateRequest;
use Illuminate\Support\Str;
use App\Models\Role;
use App\Models\User;
use App\Models\TimePlan;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Auth::user()
            ->company
            ->team()
            ->orderBy('created_at', 'desc')->get();

        return view('backoffice.team.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $timezones = ['' => ' - - - - - - '];
        $tzlist = \DateTimeZone::listIdentifiers(\DateTimeZone::ALL);
        foreach($tzlist as $item){
            $timezones[$item] = $item;
        }
        $selected = "Europe/London";
        return view('backoffice.team.create_edit', compact('timezones', 'selected'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StaffRequest $request)
    {
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->company_id = Auth::user()->company->id;
        $user->password = bcrypt($request->password);
        $user->description = $request->description;
        $user->timezone =  $request->timezone;
        $user->slug = Str::slug($request->name, '-');
        if ($user->save()) {
            $user->slug = Str::slug($request->name, '-') . '-'. $user->id;
            $user->save();

            activity()
            ->causedBy($user)
            ->log(Auth::user()->name .' created the profile for '. $user->name);  

            $user->attachRole(Role::where('name', 'team_member')->first());
            return redirect(route('backoffice.team.edit', $user->id))->with('success', 'New staff has been saved!');
        }
        $error = $user->errors()->all(':message');
        return redirect(route('backoffice.team.create'))->with('error', $error)->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $company_id = Auth::user()->company->id;
        if ($company_id != $user->company_id) {
            abort(403);
        }
        $timezones = ['' => ' - - - - - - '];
        $tzlist = \DateTimeZone::listIdentifiers(\DateTimeZone::ALL);
        foreach($tzlist as $item){
            $timezones[$item] = $item;
        }        
        return view('backoffice.team.create_edit', compact('user', 'timezones'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(StaffUpdateRequest $request, User $user)
    {
        $company_id = Auth::user()->company->id;
        if ($company_id != $user->company_id) {
            abort(403);
        }
        $user->name = $request->name;
        $user->email = $request->email;
        $user->company_id = $company_id;
        $user->timezone =  $request->timezone;
        $user->description = $request->description;
        $user->password = bcrypt($request->password);
        $user->slug = Str::slug($request->name, '-') . '-'. $user->id;
        if ($user->save()) {

            activity()
            ->causedBy($user)
            ->log(Auth::user()->name .' updated the profile of '. $user->name);  

            return redirect(route('backoffice.team.edit', $user->id))->with('success', 'Staff has been updated!');
        }
        $error = $user->errors()->all(':message');
        return redirect(route('backoffice.team.create'))->with('error', $error)->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function upload(User $user)
    {
        $company_id = Auth::user()->company->id;
        if ($company_id != $user->company_id) {
            abort(403);
        }
 
        return view('backoffice.team.upload', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function upload_photo(PhotoUpdateRequest $request, User $user)
    {
        $company_id = Auth::user()->company->id;
        if ($company_id != $user->company_id) {
            abort(403);
        }
        $helper = new \App\Helper();
        if ($request->hasfile('image')) {
            $image = $request->file('image');
            $folder = 'uploads/users/';
            $filename = $user->slug . '-'.  $user->id .'.' . $image->getClientOriginalExtension();
            $filePath = $folder . $filename;
            $rst = $helper->upload($image, $folder, 'public', $filename);
        }

        if ($rst){
            $user->image = $filename;
            $user->save();

            activity()
            ->causedBy($user)
            ->log(Auth::user()-name .' uploaded a new profile photo for '. $user->name);   

            return redirect(route('backoffice.team.edit', $user->id))->with('success', 'Photo Uploaded!');
        }
      
        return redirect(route('backoffice.team.upload', $user->id))->with('error', 'Can\'t uploaded')->withInput();
    }

    public function appointments(User $user)
    {
        $today = Carbon::now();
        $company_id = Auth::user()->company->id;
        if ($company_id != $user->company_id) {
            abort(403);
        }

        activity()
            ->causedBy($user)
            ->log($user->name .' viewed the appointments');        

        $appointments = $user->appointments()->whereDate('start_time', '>', $today)->orderBy('start_time', 'desc')->paginate();
        return view('backoffice.team.appointments', compact('appointments', 'user'));
    }

    public function time_plan(User $user)
    {
        $days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
        $hours = [];

        Carbon::setToStringFormat('H:i');
        $startTime = Carbon::createFromFormat('H:i', '00:00');
        
        while ($startTime->format('H:i') < '23:45') {
            $h = substr($startTime->format('H:i'), 0, 5);
            $hours[$h] = $h;
            $startTime = $startTime->addMinutes(15);
        }
        $h = substr($startTime->format('H:i'), 0, 5);
        $hours[$h] = $h;

        $working_hours = $user->working_hours()->get();

        return view('backoffice.team.time-planning', compact('days', 'hours', 'working_hours', 'user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function time_plan_update(User $user, Request $request)
    {
        $days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];


        activity()
            ->causedBy($user)
            ->log($user->name .' updated the time plan');

        foreach ($days as $day) {
            $time = TimePlan::where('user_id', $user->id)
                ->where('day', $day)->first();
            if ($time) {
                $time->update(
                    [
                        'day' => $day,
                        'user_id' => $user->id,
                        'start_time' => $request->input("{$day}_start_time"),
                        'end_time' => $request->input("{$day}_end_time"),
                    ]
                );
            } else {
                TimePlan::create([
                    'day' => $day,
                    'user_id' => $user->id,
                    'start_time' => $request->input("{$day}_start_time"),
                    'end_time' => $request->input("{$day}_end_time"),
                ]);
            }

        }
        return redirect(route('backoffice.team.time_plan', $user->id))->with('success', trans('Time Planning has been updated!'));
    }
}