<?php

namespace App\Http\Controllers\BackOffice;

use App\Models\Customer;
use Illuminate\Http\Request;
use Khsing\World\Models\City;
use Khsing\World\Models\Country;
use Illuminate\Routing\Controller;
use Auth;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company_id = Auth::user()->company->id;
        $customers = Customer::whereHas('appointments', function ($query) use ($company_id) {
            $query->where("company_id", $company_id);
        })->get();

        return view('backoffice.customers.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cityData = City::where('country_id', 148)->get();

        $cities[0] = " - - - - - ";
        foreach ($cityData as $city) {
            $cities[$city->id] = $city->name;
        }

        return view('backoffice::customers.create_edit', compact('cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = new Customer;
        $customer->name = $request->name;
        $customer->email = $request->email;
        $customer->phone = $request->phone;
        $customer->address = $request->address;
        $customer->city = $request->city;
        $customer->state = $request->state;
        $customer->postcode = $request->postcode;
        $customer->country = $request->country;
        $customer->timezone = $request->timezone;
        $customer->language = $request->language;

        if ($customer->save()) {
            return redirect(route('backoffice.customers.edit', $customer->id))->with('success', trans('labels.form.success'));
        }
        $error = $customer->errors()->all(':message');
        return redirect(route('backoffice.customers.create'))->with('error', $error)->withInput();
    }
 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        $cityData = City::where('country_id', 148)->get();
        $cities[0] = " - - - - - ";
        foreach ($cityData as $city) {
            $cities[$city->id] = $city->name;
        }
        return view('backoffice::customers.create_edit', compact('customer', 'cities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        $customer->name = $request->name;
        $customer->email = $request->email;
        $customer->phone = $request->phone;
        $customer->address = $request->address;
        $customer->city = (!empty($request->city) ? $request->city : $customer->city);
        $customer->state = (!empty($request->state) ? $request->state : $customer->state);
        $customer->postcode = $request->postcode;
        $customer->country = $request->country;
        $customer->timezone = $request->timezone;
        $customer->language = $request->language;
        $customer->company_id = Auth::user()->company->id;
        if ($customer->save()) {
            return redirect(route('backoffice.customers.edit', $customer->id))->with('success', trans('labels.form.success'));
        }
        $error = $customer->errors()->all(':message');
        return redirect(route('backoffice.customers.create'))->with('error', $error)->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function appointments(Customer $customer){
        $company_id = Auth::user()->company->id;
        $appointments = $customer->appointments()->where("company_id", $company_id)->get();
        return view('backoffice::customers.appointments', compact('customer', 'appointments'));
    }
}
