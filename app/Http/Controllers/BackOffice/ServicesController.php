<?php

namespace App\Http\Controllers\BackOffice;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Service;
use App\Http\Requests\CreateRequest;
use Illuminate\Routing\Controller;
use Auth;
use Str;
use App\Http\Requests\PhotoUpdateRequest;

class ServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Auth::user()->company->services()->orderBy('created_at',  'desc')->get();
    	return view('backoffice.services.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees = Auth::user()->company->users()->orderBy('created_at',  'desc')->get();
        $durations = ['15' => '15', '30' => '30', '45' => '45', '60' => '60', '90' => '90'];
    	return view('backoffice.services.create_edit', compact('employees', 'durations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        $user = Auth::user();
        $service = new Service;
        $service->name = $request->name;
        $service->description = $request->description;
        $service->duration = $request->duration;
        $service->price = $request->price;
        $service->company_id = $user->company->id;
        if ($service->save()) {
            activity()
                ->causedBy($user)
                ->performedOn($service)
                ->log('Created');

            $service->slug = Str::slug($service->name, '-') . '-'. $service->id;
            $service->save();
            $service->team()->sync($request->staff);
            return redirect(route('backoffice.services.edit', $service->id))->with('success', 'Your new service has been recorded!');
        }
        $error = $service->errors()->all(':message');
        return redirect(route('backoffice.services.create'))->with('error', $error)->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        $company = Auth::user()->company;
        if ($company->id != $service->company_id){
            \abort(403);
        }

        $employees = $company->users()->orderBy('created_at',  'desc')->get();
        $durations = ['15' => '15', '30' => '30', '45' => '45', '60' => '60', '90' => '90'];
    	return view('backoffice.services.create_edit', compact('employees', 'service', 'durations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        $user = Auth::user();
        $service->name = $request->name;
        $service->description = $request->description;
        $service->duration = $request->duration;
        $service->price = $request->price;
        $service->company_id = Auth::user()->company->id;
        $service->slug = Str::slug($request->name, '-') . '-'. $service->id;
        if ($service->save()) {
            activity()
                ->causedBy($user)
                ->performedOn($service)
                ->log('Updated');

            $service->team()->sync($request->staff);
            return redirect(route('backoffice.services.edit', $service->id))->with('success', 'Service has been updated!');
        }
        $error = $service->errors()->all(':message');
        return redirect(route('backoffice.services.edit', $service->id))->with('error', $error)->withInput();
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Service $service)
    {
        $company = Auth::user()->company;
        if ($company->id != $service->company_id){
            \abort(403);
        }

        $employees = $company->users()->orderBy('created_at',  'desc')->get();
        $durations = ['15' => '15', '30' => '30', '45' => '45', '60' => '60', '90' => '90'];
    	return view('backoffice.services.delete', compact('employees', 'service', 'durations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Service $service)
    {
        $user = Auth::user();
        activity()
            ->causedBy($user)
            ->performedOn($service)
            ->log('Deleted');        
        $service->delete();
        return redirect(route('backoffice.services.index'))->with('success', 'The service has been removed!')->withInput();
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function upload(Service $service)
    {

        return view('backoffice.services.upload', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function upload_photo(PhotoUpdateRequest $request, Service $service)
    {
 
        $helper = new \App\Helper();
        if ($request->hasfile('image')) {
            $image = $request->file('image');
            $folder = 'uploads/services/';
            $filename = $service->slug . '-'.  $service->id .'.' . $image->getClientOriginalExtension();
            $filePath = $folder . $filename;
            $rst = $helper->upload($image, $folder, 'public', $filename);
        }
 
        if ($rst){
            $service->image = $filename;
            $service->save();
            return redirect(route('backoffice.services.edit', $service->id))->with('success', 'Photo Uploaded!');
        }
      
        return redirect(route('backoffice.services.upload', $service->id))->with('error', 'Can\'t uploaded')->withInput();
    }

}