<?php

namespace App\Http\Controllers\BackOffice;

use App\Http\Requests\AccountRequest;
use App\Models\Company;
use App\Models\Template;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Intervention\Image\Facades\Image;
use Khsing\World\Models\City;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company = Auth::user()->company;
        $templates = [];
        $templates['basic'] = 'Basic';
        return view('backoffice.account.index', compact('company', 'templates'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AccountRequest $request)
    {
        $company = Auth::user()->company;
        $company->name = $request->name;
        $company->description = $request->description;
        $company->address = $request->address;
        $company->postcode = $request->postcode;
        $company->email = $request->email;
        $company->phone = $request->phone;
        $company->website = $request->website;
        $company->auto_renew = $request->auto_renew;
        $company->template_name = $request->template;
        if ($company->save()) {
            return redirect(route('backoffice.account'))->with('success', trans('Your account has been updated'));
        }
        $error = $company->errors()->all(':message');
        return redirect(route('backoffice.account'))->with('error', $error)->withInput();
    }

    public function orders()
    {
        $company = Auth::user()->company;
        return view('backoffice.account.orders', compact('company'));
    }

    public function template_save(Request $request)
    {
        $company = Auth::user()->company;
        $company->template_id = $request->template_id;
        $company->save();
        return redirect(route('backoffice.account.customize'))->with('success', trans('Your template has been updated'));
    }

    public function customize()
    {
        $company = Auth::user()->company;
        $templateDatas = Template::all();

        $templates[0] = " - - - - - ";
        foreach ($templateDatas as $data) {
            $templates[$data->id] = $data->name;
        }

        return view('backoffice.account.customize', compact('company', 'templates'));
    }

    public function template(Request $request)
    {
        $company = Auth::user()->company;
        $company->template = $request->template;
        if ($company->save()) {
            return redirect(route('backoffice.account.customize'))->with('success', trans('Your account has been updated'));
        }
    }

    public function upload(Request $request)
    {
        $user = Auth::user();
        $company = $user->company;
        $file = $request->croppedImage;
        $img = Image::make($file);

        $types = ['image/jpeg' => 'jpg', 'image/jpg' => 'jpg', 'image/png' => 'png', '	image/gif' => 'gif'];
        $type = (isset($types[$img->mime()])) ? $types[$img->mime()] : 'jpg';
        $image_name = time() . "." . $type;
        \Storage::disk('local')->put('public/company/images/' . $company->id . '/' . (string) $image_name, $img->encode(), 'public');

        $company->logo = $image_name;
        if ($company->save()) {
            return asset('storage/company/images/' . $company->id . '/' . $image_name);
        }
        return "There are some problems with your submission!";
    }
}
