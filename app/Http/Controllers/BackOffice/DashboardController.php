<?php

namespace App\Http\Controllers\BackOffice;

use App\Models\User;
use App\Models\Company;
use Auth;
use Carbon\Carbon; 

/**
 * Class DashboardController.
 */
class DashboardController extends BaseController
{

    public function __construct()
    {
         parent::__construct();
    }    
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $user = Auth()->user();

        $current_user = $user;
        if ($user->hasRole('administrator')) {
/*             $users = User::whereHas(['roles' => function($q){
                $q->where('name', 'owner');
            }])->with('company:id,name')->orderBy('created_at', 'desc')->paginate(100);
 */
            $companies = Company::with('owner')->orderBy('created_at', 'desc')->paginate(100);

            return view('admin.dashboard', compact('companies'));
        }

        activity()
            ->causedBy($user)
            ->log('User had been acced to the dashboard!');

        $time = (isset($request->time) ? $request->time : 'next');
        $approved = (isset($request->approved) ? $request->approved : 1);

        $today = Carbon::now();
        $upcoming_appointments = $user
            ->company
            ->appointments()
            ->whereDate('start_time', '>', $today)
            ->where('approved', '=', 1)
            ->where(function ($query) use ($user){
                if (!$user->hasRole('owner')){
                    $query->where('user_id', $user->id);
                }
            })
            ->orderBy('start_time', 'desc')->limit(10)->get();

        $unapproved_appointments = $user
            ->company
            ->appointments()
            ->whereDate('start_time', '>=', $today)
            ->where('approved', 0)
            ->where(function ($query) use ($user){
                if (!$user->hasRole('owner')){
                    $query->where('user_id', $user->id);
                }
            })->with('staff:id,name')->with('service:id,name')            
            ->orderBy('start_time', 'desc')->get();

        $today_appointments = $user
            ->company
            ->appointments()
            ->whereDate('start_time', '=', $today->toDateString())
            ->where('approved', '=', 1)
            ->where(function ($query) use ($user){
                if (!$user->hasRole('owner')){
                    $query->where('user_id', $user->id);
                }
            })->with('staff:id,name')            
            ->orderBy('start_time', 'desc')->take(10)->get();
        return view('backoffice.dashboard', compact('upcoming_appointments', 'today_appointments', 'unapproved_appointments', 'current_user'));
    }
}