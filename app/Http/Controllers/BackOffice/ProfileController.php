<?php

namespace App\Http\Controllers\BackOffice;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Http\Requests\ProfileRequest;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        return view('backoffice.profile', compact('user'));
    }

    public function save(ProfileRequest $request)
    {
        $user = Auth::user();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        activity()
        ->causedBy($user)
        ->log($user->name . 'Updated the profile');    

        return redirect(route('backoffice.profile'))->with('success', trans('Your profile has been updated!'));
    }
}
