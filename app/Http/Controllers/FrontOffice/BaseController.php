<?php

namespace App\Http\Controllers\FrontOffice;

use App\Models\Company;
use Illuminate\Routing\Controller;

class BaseController extends Controller
{
    public $company;
    public function __construct()
    {
        $this->company = Company::first();
    }
}
