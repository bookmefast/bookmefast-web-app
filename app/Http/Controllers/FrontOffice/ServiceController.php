<?php

namespace App\Http\Controllers\FrontOffice;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Models\Company;
use App\Models\Service;
use App\Models\User;

class ServiceController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $company = $this->company;
        return view("frontoffice.{$company->template_name}.services", compact('company'));
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function show(Service $service)
    {
        $company = $this->company;
        return view("frontoffice.{$company->template_name}.service_view", compact('company', 'service'));
    }
}
