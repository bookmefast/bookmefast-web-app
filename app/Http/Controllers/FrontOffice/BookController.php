<?php

namespace App\Http\Controllers\FrontOffice;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Company;
use App\Models\Service;
use App\Models\User;

class BookController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(User $user, Service  $service)
    {
        $company = $this->company;
        $timezones = ['' => ' - - - - - - '];
        $tzlist = \DateTimeZone::listIdentifiers(\DateTimeZone::ALL);
        foreach($tzlist as $item){
            $timezones[$item] = $item;
        }        
        $selected = "Europe/London";
        return view("frontoffice.{$company->template_name}.book", compact('company', 'user', 'service', 'timezones', 'selected'));
    }
  
}
