<?php

namespace App\Http\Controllers\FrontOffice;

use App\Models\Company;
use Illuminate\Http\Response;

class PageController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $company = $this->company;
        return view("frontoffice.{$company->template_name}.home", compact('company'));
    }
}
