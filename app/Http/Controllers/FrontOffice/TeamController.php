<?php

namespace App\Http\Controllers\FrontOffice;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Models\Company;
use App\Models\Service;
use App\Models\User;

class TeamController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function index()
    {
        $company = $this->company;
        return view("frontoffice.{$company->template_name}.team", compact('company'));
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function member_view(User $user)
    {
        $company = $this->company;
        return view("frontoffice.{$company->template_name}.user_view", compact('company', 'user'));
    }
}
