<?php
namespace App\Http\Controllers\Api;

use App\Helper;
use App\Http\Requests\BookSaveRequest;
use App\Mail\AppointmentReceivedToCompany;
use App\Mail\AppointmentReceivedToCustomer;
use App\Mail\AppointmentReceivedToStaff;
use App\Models\Appointment;
use App\Models\Company;
use App\Models\Service;
use App\Models\User;
use App\Transformers\ServiceTransformer;
use App\Transformers\WorkingHourTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use Mail;
use App\Http\Requests\LocalApiRequest as ApiRequest;
use DB;

class BookController extends Controller
{
    public function save(BookSaveRequest $request)
    {
        $service_id = $request->input('service');
        $company_id = $request->input('company_id');
        $user_id = $request->input('user_id');
        $selected_date = $request->input('selected_date');
        $selected_hour = $request->input('selected_hour');
        $timezone = $request->input('timezone');
        $company = Company::find($company_id);
        $service = Service::find($service_id);

        $start_time = \Carbon\Carbon::create(substr($selected_date, 0, 4), substr($selected_date, 5, 2), substr($selected_date, 8, 2), substr($selected_hour, 0, 2), substr($selected_hour, 3, 2));
        $end_time = \Carbon\Carbon::create(substr($selected_date, 0, 4), substr($selected_date, 5, 2), substr($selected_date, 8, 2), substr($selected_hour, 0, 2), substr($selected_hour, 3, 2));
        $end_time = $end_time->addMinutes($service->duration);

        $appointment = new Appointment;
        $appointment->service_id = $service->id;
        $appointment->user_id = $user_id;
        $appointment->company_id = $company->id;
        $appointment->customer_name = $request->input('name');
        $appointment->customer_email = $request->input('email');
        $appointment->customer_phone = $request->input('phone');
        $appointment->start_time = $start_time;
        $appointment->end_time = $end_time;
        $appointment->timezone = $timezone;
        if ($appointment->save()) {
            $user = User::find($user_id);
            DB::table('usages')
                ->where('company_id', $user->company->id)
                ->whereRaw('(CURRENT_DATE BETWEEN start_date AND end_date)')
                ->update(['counted' => 'counted + 1']);
    
            Mail::to($appointment->customer_email)
                ->queue(new AppointmentReceivedToCustomer($appointment));

            Mail::to($appointment->staff->email)
                ->queue(new AppointmentReceivedToStaff($appointment));

            Mail::to($company->company_email)
                ->queue(new AppointmentReceivedToCompany($appointment));

            $appointment->notify = 1;
            $appointment->save();

            return response()->json([
                'appointment' => $appointment->id,
            ], 201);
        }
    }

    public function check_availability(ApiRequest $request)
    {
        $service = Service::find($request->input('service'));
        $company = Company::find($request->input('company_id'));
        $user = User::find($request->input('user_id'));

        if ($service->company_id != $company->id ){
            return response('Request is not valid', 404);
        }

        if (!$user->services()->where('service_id',  $service->id)->first()){
            return response('Service is not valid', 404);
        }

        $timezone = $request->input('timezone');
        $dt = $request->input('date');

        $date = Carbon::create(substr($dt, 0, 4), substr($dt, 5, 2), substr($dt, 8, 2));
        $duration = $service->duration;

        $day = $date->format('l');
        $workingHours = $user->working_hours()->where('day', $day)->first();

        $timePlan = [];
        $workingHourArray = [];
        if ($workingHours) {
            $start_workDay = \Carbon\Carbon::create(substr($dt, 0, 4), substr($dt, 5, 2), substr($dt, 8, 2), substr($workingHours->start_time, 0, 2), substr($workingHours->start_time, 3, 2), $user->timezone);
            $end_workDay = \Carbon\Carbon::create(substr($dt, 0, 4), substr($dt, 5, 2), substr($dt, 8, 2), substr($workingHours->end_time, 0, 2), substr($workingHours->end_time, 3, 2), $user->timezone);

            while ($start_workDay < $end_workDay) {
                $start = clone $start_workDay;
                $start_workDay->addMinutes($duration);

                $timePlan[] = [
                    'start' => $start->format('H:i'),
                    'end' => $start_workDay->format('H:i'),

                    'utc_timezone_start' => Helper::convert($start, 'UTC', $user->timezone, 'H:i'),
                    'utc_timezone_end' => Helper::convert($start_workDay, 'UTC', $user->timezone, 'H:i'),

                    'timezone_start' => Helper::convert($start, $timezone, $user->timezone, 'H:i'),
                    'timezone_end' => Helper::convert($start_workDay, $timezone, $user->timezone, 'H:i'),
                    'date_time' => Helper::convert($start, $timezone, $user->timezone, 'd M Y'),
                    'available' => 0,
                ];
            }
/* 
            $appointments = $service->appointments()
                ->where('user_id', $user->id)
                ->whereDate('start_time', $date)
                ->whereRaw('(approved = 1 OR (approved = 0 and (TIMESTAMPDIFF(MINUTE,created_at, NOW())) < 60)) ')
                ->get();

            foreach ($appointments as $appointment) {
                $start = array_search($appointment->start_time->format('H:i'), array_column($timePlan, 'utc_timezone_start'));
                $end = array_search($appointment->end_time->format('H:i'), array_column($timePlan, 'utc_timezone_end'));

                for ($i = $start; $i <= $end; $i++) {
                    $timePlan[$i]['available'] = 1;
                }
            } */

            $resourceWH = new Item($workingHours, new WorkingHourTransformer());
            $fractal = new Manager();
            $workingHourArray = $fractal->createData($resourceWH)->toArray();
        }

        $resource = new Item($service, new ServiceTransformer());
        $fractal = new Manager();
        $array = $fractal->createData($resource)->toArray();

        $result = [
            'service' => $array,
            'timePlan' => $timePlan,
            'workingHours' => $workingHourArray,
        ];
        return $result;
    }

    public function saveTest(BookSaveRequest $request)
    {
        $service_id = $request->input('service');
        $company_id = $request->input('company');
        $user_id = $request->input('user');
        $selected_date = $request->input('selected_date');
        $selected_hour = $request->input('selected_hour');
        $timezone = $request->input('timezone');
        $company = Company::find($company_id);
        $service = Service::find($service_id);

        $start_time = \Carbon\Carbon::create(substr($selected_date, 0, 4), substr($selected_date, 5, 2), substr($selected_date, 8, 2), substr($selected_hour, 0, 2), substr($selected_hour, 3, 2));
        $end_time = \Carbon\Carbon::create(substr($selected_date, 0, 4), substr($selected_date, 5, 2), substr($selected_date, 8, 2), substr($selected_hour, 0, 2), substr($selected_hour, 3, 2));
        $end_time = $end_time->addMinutes($service->duration);

        $appointment = new Appointment;
        $appointment->service_id = $service->id;
        $appointment->user_id = $user_id;
        $appointment->company_id = $company->id;
        $appointment->customer_name = $request->input('name');
        $appointment->customer_email = $request->input('email');
        $appointment->customer_phone = $request->input('phone');
        $appointment->start_time = $start_time;
        $appointment->end_time = $end_time;
        $appointment->timezone = $timezone;
        if ($appointment->save()) {

            Mail::to($appointment->customer_email)
                ->queue(new AppointmentReceivedToCustomer($appointment));

            Mail::to($appointment->staff->email)
                ->queue(new AppointmentReceivedToStaff($appointment));

            $appointment->notify = 1;
            $appointment->save();

            return response()->json([
                'appointment' => $appointment->id,
            ], 200);
        }
    }

}