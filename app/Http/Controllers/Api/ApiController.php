<?php
namespace App\Http\Controllers\Api;

use App\Helper;
use App\Models\Company;
use App\Models\Service;
use App\Models\User;
use App\Transformers\CompanyTransformer;
use App\Transformers\ServiceTransformer;
use App\Transformers\UserTransformer;
use App\Transformers\WorkingHourTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class ApiController extends Controller
{
    public function check_availability(Request $request)
    {
        $company = $request->user();

        $service = Service::find($request->input('service'));
        $user = $company->owner()->first();

        if ($service->company_id != $company->id) {
            return response()->json('Request is not valid', 404);
        }

        if (!$user->services()->where('service_id', $service->id)->first()) {
            return response()->json('Service is not valid', 404);
        }

        $timezone = $request->input('timezone');
        $dt = $request->input('date');

        $date = Carbon::create(substr($dt, 0, 4), substr($dt, 5, 2), substr($dt, 8, 2));
        $duration = $service->duration;

        $day = $date->format('l');
        $workingHours = $user->working_hours()->where('day', $day)->first();

        $timePlan = [];
        $workingHourArray = [];
        if ($workingHours) {
            $start_workDay = \Carbon\Carbon::create(substr($dt, 0, 4), substr($dt, 5, 2), substr($dt, 8, 2), substr($workingHours->start_time, 0, 2), substr($workingHours->start_time, 3, 2), $user->timezone);
            $end_workDay = \Carbon\Carbon::create(substr($dt, 0, 4), substr($dt, 5, 2), substr($dt, 8, 2), substr($workingHours->end_time, 0, 2), substr($workingHours->end_time, 3, 2), $user->timezone);


            while ($start_workDay < $end_workDay) {

                $start = clone $start_workDay;
                $start_workDay->addMinutes($duration);

                $timePlan[] = [
                    'start' => $start->format('H:i'),
                    'end' => $start_workDay->format('H:i'),

                    'utc_timezone_start' => Helper::convert($start, 'UTC', $user->timezone, 'H:i'),
                    'utc_timezone_end' => Helper::convert($start_workDay, 'UTC', $user->timezone, 'H:i'),

                    'timezone_start' => Helper::convert($start, $timezone, $user->timezone, 'H:i'),
                    'timezone_end' => Helper::convert($start_workDay, $timezone, $user->timezone, 'H:i'),
                    'date_time' => Helper::convert($start, $timezone, $user->timezone, 'd M Y'),
                    'available' => 0,
                ];
            }

            $appointments = $service->appointments()
                ->where('user_id', $user->id)
                ->whereDate('start_time', $date)
                ->whereRaw('(approved = 1 OR (approved = 0 and (TIMESTAMPDIFF(MINUTE,created_at, NOW())) < 60)) ')
                ->get();

            foreach ($appointments as $appointment) {
                $start = array_search($appointment->start_time->format('H:i'), array_column($timePlan, 'utc_timezone_start'));
                $end = array_search($appointment->end_time->format('H:i'), array_column($timePlan, 'utc_timezone_end'));

                for ($i = $start; $i <= $end; $i++) {
                    $timePlan[$i]['available'] = 1;
                }
            }

            $resourceWH = new Item($workingHours, new WorkingHourTransformer());
            $fractal = new Manager();
            $workingHourArray = $fractal->createData($resourceWH)->toArray();
        }

        $resource = new Item($service, new ServiceTransformer());
        $fractal = new Manager();
        $array = $fractal->createData($resource)->toArray();

        $result = [
            'service' => $array,
            'timePlan' => $timePlan,
            'workingHours' => $workingHourArray,
        ];
        return $result;
    }

    public function book_appointment(Request $request)
    {
        return $request->user();
    }

    public function company(Request $request)
    {
        $company = $request->user();

        $fractal = new Manager();
        if (!empty($request->input('include'))) {
            $fractal->parseIncludes($request->input('include'));
        }

        $resource = new Item($company, new CompanyTransformer());
        $array = $fractal->createData($resource)->toArray();
        return response()->json($array, 200);
    }

    public function services(Request $request)
    {
        $company = $request->user();
        $services = $company->services;

        $resource = new Collection($services, new ServiceTransformer());
        $fractal = new Manager();
        $array = $fractal->createData($resource)->toArray();

        return response()->json($array, 200);
    }

    public function users(Request $request)
    {
        $company = $request->user();
        $users = $company->team;

        $resource = new Collection($users, new UserTransformer());
        $fractal = new Manager();
        $array = $fractal->createData($resource)->toArray();

        return response()->json($array, 200);
    }

}
