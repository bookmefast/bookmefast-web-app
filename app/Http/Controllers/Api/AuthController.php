<?php
namespace App\Http\Controllers\Api;

use App\Mail\UserRegistered;
use App\Models\Company;
use App\Models\Plan;
use App\Models\Role;
use App\Models\Service;
use App\Models\Subscription;
use App\Models\TimePlan;
use App\Models\User;
use App\Transformers\UserTransformer;
use Carbon\Carbon;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use Mail;
use Str;

class AuthController extends Controller
{
    public function token(Request $request)
    {
        $company = Company::where('email', $request->email)->first();
        $token = Str::random(60);
        $company->api_token = hash('sha256', $token);
        $company->save();
        return response()->json(['token' => $token], 200);
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        $user = $this->create($request->all());

        $resource = new Item($user, new UserTransformer());
        $fractal = new Manager();
        $array = $fractal->createData($resource)->toArray();

        return response()->json($array, 200);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6'],
        ]);
    }

    protected function create(array $data)
    {
        $plan = Plan::where('amount', 0)->first();
        $api_token = Str::random(60);
        $slug = Str::random(4);
        $company = Company::create(['name' => $data['name'], 'email' => $data['email'], 'slug' => str_slug($data['name'] . ' ' . $slug), 'status' => 0, 'template_name' => 'basic']);

        $user = new User;

        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        $user->company_id = $company->id;
        $user->slug = str_slug($data['name'] . ' ' . $slug);
        $user->email_verified_at = Carbon::now();
        $user->save();

        $service = new Service;
        $service->name = 'Guitar Lesson';
        $service->description = '';
        $service->duration = 60;
        $service->price = 0;
        $service->company_id = $company->id;
        $service->slug = Str::slug('guitar-lesson', '-') . '-' . $user->id;
        $service->save();
        $service->team()->sync($user->id);
        $days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

        foreach ($days as $day) {
            $startTime = Carbon::createFromFormat('H:i', '00:00');
                 TimePlan::create([
                    'day' => $day,
                    'user_id' => $user->id,
                    'start_time' => '00:00',
                    'end_time' => '24:00',
                ]);
         }

        activity()
            ->causedBy($user)
            ->log('ApiUserRegister');

        $role = Role::where('name', '=', 'owner')->first();
        $user->attachRole($role);

        $now = Carbon::now(new \DateTimeZone('UTC'));

        Subscription::create([
            'plan_name' => $plan->name,
            'status' => 1,
            'paid' => 0,
            'amount' => $plan->amount,
            'company_id' => $company->id,
            'start_date' => $now,
            'end_date' => $now->addYear(),
        ]);
        \Mail::to(env('MAIL_FROM_ADDRESS'))->queue(new UserRegistered($user));
        return $user;
    }

}
