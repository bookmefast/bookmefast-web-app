<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Auth;

class AccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $company = Auth::user()->company;
        return [
            'name' => 'required|min:6|max:200|unique:companies,name,' . $company->id,
            'slug' => 'required|min:4|max:30|unique:companies,slug,' . $company->id,
        ];
    }
}