<?php
 
namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Khsing\World\Models\City;
use Khsing\World\Models\Country;
use Khsing\World\Models\Division;
use App\Notifications\CustomerResetPasswordNotification;

class Customer extends Authenticatable
{
    use Notifiable;
    protected $guard = 'customer';

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new CustomerResetPasswordNotification($token));
    }

    public function getCountryNameAttribute()
    {
        if ($this->country) {
            return Country::find($this->country)->name;
        }
        return;
    }

    public function getCityNameAttribute()
    {
        if ($this->city) {
            return City::find($this->city)->name;
        }
        return;
    }

    public function getStateNameAttribute()
    {
        if ($this->state) {
            return Division::find($this->state)->name;
        }
        return;
    }

    public function appointments(){
        return $this->hasMany(Appointment::class);
    }
}
