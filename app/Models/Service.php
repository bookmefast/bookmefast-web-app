<?php
 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Service extends Model
{
    use SoftDeletes;
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function team()
    {
        return $this->belongsToMany(User::class, 'service_team')
                ->withPivot(['user_id', 'service_id'])
                ->withTimestamps();
    }

    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }

    public function getServicePriceAttribute()
    {
        setlocale(LC_MONETARY, 'en_GB');
        return money_format("%i", $this->price);
    }
}