<?php
 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
use App\Helper;

class Appointment extends Model
{
    use SoftDeletes;

    protected $dates = ['start_time', 'end_time'];

    public function history()
    {
        return $this->hasMany(AppointmentHistory::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function service()
    {
        return $this->belongsTo(Service::class)->withTrashed();
    }

    public function staff()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
	
    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }	

    public function getAppointmentTimeAttribute()
    {
        $start_time = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->start_time);
        $end_time = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->end_time);
        return $start_time->format('H:i') . " - " . $end_time->format('H:i') . "<br/><i>" . $start_time->toFormattedDateString() . "</i>";
    }

    public function getAppointmentTimeUserAttribute()
    {
        $timezone = Auth::user()->timezone;
        $date = Helper::convert($this->start_time, $timezone, 'UTC', 'd M Y');
        $start_time = Helper::convert($this->start_time, $timezone, 'UTC', 'H:i');
        $end_time = Helper::convert($this->end_time, $timezone, 'UTC', 'H:i');
        return  $start_time . ' - ' . $end_time  . '<br/>'.  $date ."</i>";
    }    

    public function getIsApprovedStyleAttribute()
    {
        if ($this->approved) {
            return "text-success";
        }
        return "text-warning";
    }

    public function getIsApprovedAttribute()
    {
        if ($this->approved) {
            return "Approved";
        }
        return "Pending";
    }

    public function getStatusMessageAttribute()
    {
        if ($this->status) {
            return "Estimated";
        }
        return "Canceled";
    }    
}