<?php
 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AppointmentHistory extends Model
{
    public $timestamps = true;
    protected $dates = ['created_at'];

    public function appointment()
    {
        return $this->belongsTo(Appointment::class);
    }
}