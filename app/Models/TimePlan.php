<?php
 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TimePlan extends Model
{
    protected $guarded = array('id');  // Important
    protected $table = "working_hours";    
    protected $fillable = ['user_id', 'day', 'start_time', 'end_time'];

}