<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Customer;
use App\Models\Service;
use App\Models\TimePlan;
use App\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;
    
    protected $fillable = ['name', 'package_id', 'token', 'status', 'email'];

    public function getAddressInfoAttribute()
    {
        $text = null;
        if ($this->address) {
            $text .= $this->address . ' ';
        }

        if ($this->city_name) {
            $text .= $this->city_name . ' ';
        }

        if ($this->postcode) {
            $text .= $this->postcode . ' ';
        }
        return $text;
    }

    public function getTemplateFileNameAttribute()
    {
        if (isset($this->template)) {
            return $this->template->file_name;
        }
        return "default";
    }

    public function getLastOrderAttribute()
    {
        $order = $this->orders->last();
        if ($order) {
            return $order;
        }
        return;
    }

    public function customers()
    {
        return $this->hasMany(Customer::class);
    }

    public function services()
    {
        return $this->hasMany(Service::class);
    }

    public function team()
    {
        return $this->hasMany(User::class)->whereHas('roles', function ($query) {
            $query->whereIn('name', ['team_member', 'owner']);
        });
    }

    public function owner_name()
    {
        if ($this->owner){
            return $this->owner->name;
        }
        return;
    }

    public function owner()
    {
        return $this->hasMany(User::class)->whereHas('roles', function ($query) {
            $query->where('name', 'owner');
        });
    }

    public function getOwnerNameAttribute()
    {
        if (isset($this->owner->first()->name)) {
            return $this->owner->first()->name;
        }
        return "";
    }

    public function getOwnerEmailAttribute()
    {
        if (isset($this->owner->first()->email)) {
            return $this->owner->first()->email;
        }
        return "";
    }

    public function getCompanyEmailAttribute()
    {
        if (empty($this->email)) {
            return $this->owner->first()->email;
        }
        return $this->email;
    }


    public function users()
    {
        return $this->hasMany(User::class);
    }
 
    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function reports()
    {
        return $this->hasMany(Report::class);
    }

    public function template()
    {
        return $this->belongsTo(Template::class);
    }
 
    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }

    public function getCountryNameAttribute()
    {
        if ($this->country) {
            return Country::find($this->country)->name;
        }
        return;
    }

    public function getCityNameAttribute()
    {
        if ($this->city) {
            return City::find($this->city)->name;
        }
        return;
    }

    public function getStateNameAttribute()
    {
        if ($this->state) {
            return Division::find($this->state)->name;
        }
        return;
    }

    public function getAddressBoxAttribute()
    {
        $address = $this->address . "<br/>" . $this->postcode . "<br/>" . $this->city_name . " " . $this->city_name . " " . $this->country_name;
        return $address;
    }
/*
public static function boot()
{
parent::boot();

static::saving(function ($model) {
$model->slug = str_slug($model->name);
});

}

public function getRouteKeyName()
{
return 'slug';
} */
}
