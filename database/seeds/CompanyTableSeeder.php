<?php

use Carbon\Carbon;
use Database\DisableForeignKeys;
use Database\TruncateTable;
use Illuminate\Database\Seeder;
use App\Models\Company;


class CompanyTableSeeder extends Seeder
{
    use TruncateTable, DisableForeignKeys;

    public function run()
    {
        $this->disableForeignKeys();
        $this->truncate('companies');
        Company::create(['name' => 'My Business',  'api_token' => '', 'timezone' => 'Europe/London']);
        $this->enableForeignKeys();
    }
}