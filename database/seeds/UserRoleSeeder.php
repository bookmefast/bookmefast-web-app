<?php

use Database\DisableForeignKeys;
use Database\TruncateTable;
use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\User;
use App\Models\Company;

use Carbon\Carbon;

/**
 * Class UserRoleSeeder.
 */
class UserRoleSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();
        $this->truncate('role_user');
        $this->truncate('users');

        $company = Company::first();
        $slug = Str::random(40);
        $user = User::create(['name' => 'owner', 'email' => 'owner@example.org', 'slug' => $slug, 'company_id' => $company->id, 'password' => Hash::make('user'), 'created_at' => new DateTime(), 'updated_at' => new DateTime(), 'email_verified_at' => new DateTime()]);        
        $user->attachRole(Role::where('name', 'owner')->first());
        $this->enableForeignKeys();
    }
}