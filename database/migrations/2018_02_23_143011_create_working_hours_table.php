<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkingHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('working_hours', function (Blueprint $table) {
            $table->increments('id');
            $table->string('day');
            $table->string('start_time');
            $table->string('end_time');
            $table->integer('user_id')->nullable()->unsigned();
            $table->foreign('user_id')->references('id')->onDelete('cascade')->on('users');                                                
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('working_hours', function (Blueprint $table) {
            Schema::dropIfExists('working_hours');
        });
    }
}
