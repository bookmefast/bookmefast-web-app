<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('service_id')->nullable()->unsigned();
            $table->foreign('service_id')->references('id')->onDelete('cascade')->on('services');
            $table->integer('user_id')->nullable()->unsigned();
            $table->foreign('user_id')->references('id')->onDelete('cascade')->on('users');
            $table->integer('company_id')->nullable()->unsigned();
            $table->foreign('company_id')->references('id')->onDelete('cascade')->on('companies');                      
            $table->datetime('start_time')->nullable();
            $table->datetime('end_time')->nullable();
            $table->string('timezone')->default('Europe/London');
            $table->string('notes')->nullable();
            $table->string('verification_token')->nullable();
            $table->integer('customer_id')->nullable();
            $table->integer('approved')->default(0);
            $table->string('customer_phone')->nullable();           
            $table->string('customer_name')->nullable();
            $table->string('customer_email')->nullable();
            $table->string('customer_address')->nullable();
            $table->string('customer_city')->nullable();
            $table->string('customer_postcode')->nullable();
            $table->tinyInteger('status')->default(0)->nullable();
            $table->tinyInteger('notify')->default(0)->nullable();            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
