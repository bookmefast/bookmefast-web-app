<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('company_id')->nullable()->unsigned();
            $table->foreign('company_id')->references('id')->onDelete('cascade')->on('companies');
            $table->integer('counted')->nullable()->unsigned()->default(0);
            $table->datetime('start_date')->nullable();
            $table->datetime('end_date')->nullable();                        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usages');
    }
}
