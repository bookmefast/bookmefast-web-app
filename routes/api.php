<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

$api_version = config('api.api_version');
Route::group(["prefix" => "/{$api_version}", 'namespace' => '\App\Http\Controllers\Api'], function () {
    Route::post('/book/check-availability', ['as' => 'booking.check', 'uses' => 'BookController@check_availability']);
    Route::post('/book/save', ['as' => 'booking.save', 'uses' => 'BookController@save']);

    Route::post('/auth/register', ['as' => 'register', 'uses' => 'AuthController@register']);
    Route::post('/auth/token', ['as' => 'register', 'uses' => 'AuthController@token']);

});

Route::group(["prefix" => "{$api_version}", 'namespace' => '\App\Http\Controllers\Api', 'middleware' => ['auth:api']], function () {
    Route::post('/availability', ['as' => 'api.availability', 'uses' => 'ApiController@check_availability']);
    Route::get('/company', ['as' => 'api.services', 'uses' => 'ApiController@company']);
    Route::get('/services', ['as' => 'api.services', 'uses' => 'ApiController@services']);
    Route::get('/users', ['as' => 'api.users', 'uses' => 'ApiController@users']);
    Route::post('/appointments/book', ['as' => 'api.appointments.book', 'uses' => 'ApiController@book_appointment']);
});
