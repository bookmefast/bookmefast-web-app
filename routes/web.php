<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
use Spatie\Activitylog\Models\Activity;

Route::group(['prefix' => '/backoffice', 'as' => 'backoffice.', 'middleware' => ['web', 'auth', 'verified'], 'namespace' => '\App\Http\Controllers\BackOffice'], function () {
    Route::get('/', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);
    Route::get('/appointments', ['as' => 'appointments', 'uses' => 'AppointmentsController@index']);
    Route::get('/appointments/{appointment}/view', ['as' => 'appointments.view', 'uses' => 'AppointmentsController@show']);
    Route::post('/appointments/{appointment}/approve', ['as' => 'appointments.approve', 'uses' => 'AppointmentsController@approve']);
    Route::post('/appointments/{appointment}/cancel', ['as' => 'appointments.cancel', 'uses' => 'AppointmentsController@cancel']);
    Route::post('/appointments/{appointment}/reminder', ['as' => 'appointments.reminder', 'uses' => 'AppointmentsController@reminder']);
    Route::get('/appointments/{appointment}/update', ['as' => 'appointments.update', 'uses' => 'AppointmentsController@edit']);
    Route::post('/appointments/{appointment}/update', ['as' => 'appointments.save', 'uses' => 'AppointmentsController@save']);

    Route::get('/appointments/create', ['as' => 'appointments.create', 'uses' => 'AppointmentsController@create']);
    Route::post('/appointments/create', ['as' => 'appointments.create_app', 'uses' => 'AppointmentsController@save']);

    Route::get('/team', ['as' => 'team.index', 'uses' => 'TeamController@index']);
    Route::get('/team/create', ['as' => 'team.create', 'uses' => 'TeamController@create']);
    Route::post('/team/create', ['as' => 'team.store', 'uses' => 'TeamController@store']);
    Route::get('/team/{user}/edit', ['as' => 'team.edit', 'uses' => 'TeamController@edit']);
    Route::post('/team/{user}/edit', ['as' => 'team.update', 'uses' => 'TeamController@update']);

    Route::get('/team/{user}/upload', ['as' => 'team.upload', 'uses' => 'TeamController@upload']);
    Route::post('/team/{user}/phouploadto', ['as' => 'team.upload_photo', 'uses' => 'TeamController@upload_photo']);

    Route::get('/team/{user}/time-plan', ['as' => 'team.time_plan', 'uses' => 'TeamController@time_plan']);
    Route::post('/team/{user}/time-plan', ['as' => 'team.time_plan_update', 'uses' => 'TeamController@time_plan_update']);
    Route::get('/team/{user}/appointments', ['as' => 'team.appointments', 'uses' => 'TeamController@appointments']);

    Route::get('/account', ['as' => 'account', 'uses' => 'AccountController@index'])->middleware('can:admin-access');
    Route::post('/account', ['as' => 'account.update', 'uses' => 'AccountController@update']);

    Route::get('/profile', ['as' => 'profile', 'uses' => 'ProfileController@index']);
    Route::post('/profile', ['as' => 'profile.save', 'uses' => 'ProfileController@save']);

    Route::get('/customize', ['as' => 'customize', 'uses' => 'CustomizeController@index']);
    Route::post('/customize', ['as' => 'account.template.save', 'uses' => 'CustomizeController@template']);
    Route::post('/customize/upload', ['as' => 'account.upload', 'uses' => 'CustomizeController@upload']);
 

    Route::get('/customers', ['as' => 'customers.index', 'uses' => 'CustomersController@index']);
    Route::get('/customers/{customer}/edit', ['as' => 'customers.edit', 'uses' => 'CustomersController@edit']);
    Route::post('/customers/{customer}/edit', ['as' => 'customers.update', 'uses' => 'CustomersController@update']);
    Route::get('customers/{customer}/appointments', ['as' => 'customers.appointments', 'uses' => 'CustomersController@appointments']);

    Route::get('/services', ['as' => 'services.index', 'uses' => 'ServicesController@index']);
    Route::get('/services/create', ['as' => 'services.create', 'uses' => 'ServicesController@create']);
    Route::post('/services/create', ['as' => 'services.store', 'uses' => 'ServicesController@store']);
    Route::get('/services/{service}/edit', ['as' => 'services.edit', 'uses' => 'ServicesController@edit']);
    Route::post('/services/{service}/edit', ['as' => 'services.update', 'uses' => 'ServicesController@update']);
    Route::get('/services/{service}/delete', ['as' => 'services.delete', 'uses' => 'ServicesController@delete']);
    Route::post('/services/{service}/delete', ['as' => 'services.destroy', 'uses' => 'ServicesController@destroy']);
    Route::get('/services/{service}/upload', ['as' => 'services.upload', 'uses' => 'ServicesController@upload']);
    Route::post('/services/{service}/phouploadto', ['as' => 'services.upload_photo', 'uses' => 'ServicesController@upload_photo']);
});

Route::group(['prefix' => '/', 'as' => 'frontoffice.', 'middleware' => ['web'], 'namespace' => '\App\Http\Controllers\FrontOffice'], function () {
    Route::get('/', ['as' => 'home', 'uses' => 'PageController@index']);
    Route::get('/team', ['as' => 'team.index', 'uses' => 'TeamController@index']);
    Route::get('/services', ['as' => 'services.index', 'uses' => 'ServiceController@index']);
    Route::get('/team/{userSlug}', ['as' => 'team.member.view', 'uses' => 'TeamController@member_view']);
    Route::get('/services/{serviceSlug}', ['as' => 'service.view', 'uses' => 'ServiceController@show']);
    Route::get('/book/{userSlug}/{serviceSlug}', ['as' => 'book.create', 'uses' => 'BookController@index']);
});

Auth::routes(['verify' => true]);
